package org.university.grabovskyi.search;

import lombok.Getter;
import lombok.Setter;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.university.grabovskyi.search.windows.ResultInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class GraphVisualization {

    private int[][] adjacencyMatrix;


    private Graph graph;

    private Search search;

    private String styleSheet =
            "node {" +
                    "	fill-color: black;" +
                    "text-size : 15px;" +
                    "}" +
                    "node.marked {" +
                    "	fill-color: red;" +
                    "}";


    public GraphVisualization(){
        graph = new SingleGraph("My Graph");

        graph.addAttribute("ui.stylesheet", styleSheet);
        graph.setStrict(false);
        graph.setAutoCreate( true );
        graph.display();

        graph.addNode("root");

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }
    }

    public int[][] getAdjacencyMatrix() {
        int n = graph.getNodeCount();
        adjacencyMatrix = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                adjacencyMatrix[i][j] = graph.getNode(i).hasEdgeBetween(j) ? 1 : 0;

        return adjacencyMatrix;
    }


    public void addNode(String name){
        graph.addNode(name);
        graph.getNode(name).addAttribute("ui.label", graph.getNode(name).getId());
    }

    public void addEdge(String from, String to){
        graph.addEdge("" + from + "->" + to, from, to);
    }

    public void deleteNode(String name){
        graph.removeNode(name);
    }

    public void deleteEdge(String name){
        graph.removeEdge(name);
    }

    public void clear(){
        for (Node n : graph.getEachNode()){
            n.removeAttribute("ui.class");
        }
    }

    public List<Node> explore() {
        //Search search = new BFS(getAdjacencyMatrix(), start, finish, direction);
        Stack<Integer> route = search.search();
        List<Node> nodes = new ArrayList<>();
        search.getSteps().forEach(x -> nodes.add(graph.getNode(x)));

        for (int i=0; i<search.getLabels().length; i++){
            System.out.println(graph.getNode(i).getId() + " search number " + search.getLabels()[i]);
        }

        new ResultInfo(route, search.getLabels(), graph);

        return nodes;
    }

    public void setAdjacencyMatrix(int[][] adjacencyMatrix) {
        this.adjacencyMatrix = adjacencyMatrix;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public Search getSearch() {
        return search;
    }

    public void setSearch(Search search) {
        this.search = search;
    }

    public String getStyleSheet() {
        return styleSheet;
    }

    public void setStyleSheet(String styleSheet) {
        this.styleSheet = styleSheet;
    }
}
