package org.university.grabovskyi.search;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public abstract class DefaultSearch implements Search {

    protected int[][] graphMatrix;
    protected int startNumber;
    protected int goalNumber;
    protected Direction direction;

    protected int[] labels;
    protected int dfsCounter;

    protected List<Integer> steps;

    public DefaultSearch(int[][] matrix, int startNumber, int goalNumber, Direction direction){
        graphMatrix = matrix;
        labels = new int[matrix[0].length];
        this.startNumber = startNumber;
        this.goalNumber = goalNumber;
        this.direction = direction;
        this.steps = new ArrayList<>();
    }

    @Override
    public int[] getLabels() {
        return labels;
    }

    @Override
    public List<Integer> getSteps() {
        return steps;
    }
}
