package org.university.grabovskyi.search;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class DFS extends DefaultSearch implements Search {

    private Stack<Integer> res;

    public DFS(int[][] matrix, int startNumber, int goalNumber, Direction direction){
        super(matrix, startNumber, goalNumber, direction);
    }

    @Override
    public Stack<Integer> search(){
        res = new Stack<>();
        res.push(startNumber);
        steps.add(startNumber);
        labels[startNumber] = ++dfsCounter;
        _search(startNumber, res);
        while(!res.empty()){
            if(res.peek() == goalNumber){
                return res;
            }
            if(!_search(res.peek(), res)){
                steps.add(res.pop());
            }
        }
        return res;
    }

    private boolean _search(int number, Stack<Integer> L)
    {
        if(direction == Direction.RIGHT){
            for(int i = 0; i<graphMatrix[0].length; i++ ){
                if(labels[i]==0 && graphMatrix[number][i] == 1){
                    pushToStack(i, L);
                    return true;
                }
            }
        }else {
            for (int i = graphMatrix[0].length - 1; i >= 0; i--){
                if (labels[i] == 0 && graphMatrix[number][i] == 1){
                    pushToStack(i, L);
                    return true;
                }
            }
        }
        return false;
    }

    private void pushToStack(int i, Stack<Integer> L){
        steps.add(i);
        L.push(i);
        labels[i] = ++dfsCounter;
    }

    private void show(){
        for(int item : res){
            System.out.println(item + "\t");
        }
        System.out.println();
    }
}
