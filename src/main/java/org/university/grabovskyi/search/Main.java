package org.university.grabovskyi.search;

import org.university.grabovskyi.search.windows.Menu;

import javax.swing.*;


public class Main {
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(
                    "com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
            );
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(Menu::new);
    }
}
