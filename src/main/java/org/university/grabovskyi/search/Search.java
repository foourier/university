package org.university.grabovskyi.search;

import java.util.List;
import java.util.Stack;

public interface Search {
    Stack<Integer> search();
    List<Integer> getSteps();
    int[] getLabels();
}
