package org.university.grabovskyi.search;

import java.util.*;

public class BFS extends DefaultSearch implements Search {

    private Deque<Integer> res;
    private Map<Integer, Stack<Integer>> route;

    public BFS(int[][] matrix, int startNumber, int goalNumber, Direction direction) {
        super(matrix, startNumber, goalNumber, direction);
        route = new HashMap<>();
        Stack<Integer> first = new Stack<>();
        first.push(startNumber);
        route.put(startNumber, first);
    }

    @Override
    public Stack<Integer> search() {
        res = new LinkedList<>();
        res.add(startNumber);
        steps.add(startNumber);
        labels[startNumber] = ++dfsCounter;
        _search(startNumber, res);
        while(!res.isEmpty()){
            if(res.peekLast() == goalNumber){
                return route.get(res.peekLast());
            }
            if(!_search(res.peek(), res)){
                steps.add(res.poll());
            }
        }
        return new Stack<>();
    }



    private boolean _search(int number, Queue<Integer> L)
    {
        int count = 0;
        if(direction == Direction.LEFT){
            for(int i = 0; i<graphMatrix[0].length; i++ ){
                if(labels[i]==0 && graphMatrix[number][i] == 1){
                    pushToQueue(i, L);
                    addToRoute(number, i);
                    count++;
                    if (i == goalNumber)
                        return true;
                }
            }
        }else {
            for (int i = graphMatrix[0].length - 1; i >= 0; i--){
                if (labels[i] == 0 && graphMatrix[number][i] == 1){
                    pushToQueue(i, L);
                    addToRoute(number, i);
                    count++;
                    if (i == goalNumber)
                        return true;
                }
            }
        }

        return count != 0;
    }


    private void addToRoute(int prev, int curr){
        Stack<Integer> temp = (Stack<Integer>) route.get(prev).clone();
        temp.push(curr);
        route.put(curr, temp);
    }

    private void pushToQueue(int i, Queue<Integer> L){
        steps.add(i);
        L.add(i);
        labels[i] = ++dfsCounter;
    }

}
