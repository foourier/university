package org.university.grabovskyi.search.windows;

import org.graphstream.graph.Node;
import org.university.grabovskyi.search.BFS;
import org.university.grabovskyi.search.DFS;
import org.university.grabovskyi.search.Direction;
import org.university.grabovskyi.search.GraphVisualization;

import javax.swing.*;
import java.util.List;

public class Menu extends JFrame {

    private GraphVisualization graph;

    private JPanel contentPane;
    private JButton addNodeButton;
    private JButton deleteEdgeButton;
    private JButton addEdgeButton;
    private JButton deleteNodeButton;
    private JButton exploreButton;
    private JComboBox direction;
    private JButton clearButton;
    private JComboBox method;
    private JTextField fromNode;
    private JTextField toNode;


    public Menu() {
        super("Menu");
        setContentPane(contentPane);
        //setModal(true);
        //getRootPane().setDefaultButton(addNodeButton);
        createGraph();
        addNodeButton.addActionListener(e -> addNode());
        addEdgeButton.addActionListener(e -> addEdge());
        deleteNodeButton.addActionListener(e -> deleteNode());
        deleteEdgeButton.addActionListener(e -> deleteEdge());

        exploreButton.addActionListener(e -> explore());
        clearButton.addActionListener(e -> clearGraph());
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        for(Direction d: Direction.values())
            direction.addItem(d);


        method.addItem("DFS");
        method.addItem("BFS");

        pack();
        setVisible(true);
    }

    private void addNode() {
        new AddNode(graph);
    }

    private void addEdge() {
        new AddEdge(graph);
    }

    private void deleteNode() {
        new DeleteNode(graph);
    }

    private void deleteEdge() {
        new DeleteEdge(graph);
    }

    private void explore(){

        System.out.println();

        try {
            //DFS else BFS
            if (method.getSelectedIndex() == 0) {


                graph.setSearch(new DFS(
                        graph.getAdjacencyMatrix(),
                        graph.getGraph().getNode(
                                fromNode.getText()
                        ).getIndex(),
                        graph.getGraph().getNode(
                                toNode.getText()
                        ).getIndex(),
                        Direction.valueOf(
                                direction.getSelectedItem().toString()
                        )
                ));

            } else {
                graph.setSearch(new BFS(
                        graph.getAdjacencyMatrix(),
                        graph.getGraph().getNode(
                                fromNode.getText()
                        ).getIndex(),
                        graph.getGraph().getNode(
                                toNode.getText()
                        ).getIndex(),
                        Direction.valueOf(
                                direction.getSelectedItem().toString()
                        )
                ));
            }

        //graph.setSearch();

        List<Node> nodes = graph.explore();
        Runnable r;
        if(method.getSelectedIndex() == 0) {
             r = () -> {
                for (Node next : nodes) {
                    if (next.hasAttribute("ui.class"))
                        next.removeAttribute("ui.class");
                    else
                        next.setAttribute("ui.class", "marked");
                    sleep();
                }
            };
        }else {
            r = () -> {
                for (Node next : nodes) {
                    next.setAttribute("ui.class", "marked");
                    sleep();
                }
            };
        }


        Thread t = new Thread(r);
        t.start();
        }catch (NullPointerException ex){
            new ErrorWindow("Некоректний ввід!!!");
        }
    }

    private void clearGraph(){
        graph.clear();
    }



    protected void sleep() {
        try { Thread.sleep(1000); } catch (Exception e) {}
    }



    private void createGraph(){
        graph = new GraphVisualization();
        graph.addNode("1");
        graph.addNode("2");
        graph.addNode("3");
        graph.addNode("4");
        graph.addNode("5");
        graph.addNode("6");
        graph.addNode("7");
        graph.addNode("8");
        graph.addNode("9");
        graph.addNode("10");
        graph.addNode("11");
        graph.addNode("12");
        graph.addNode("13");
        graph.addNode("13");
        graph.addNode("14");
        graph.addNode("15");
        graph.addNode("16");
        graph.addNode("17");
        graph.addNode("18");
        graph.addNode("19");
        graph.addNode("20");
        graph.addNode("21");
        graph.addNode("22");
        graph.addNode("23");
        graph.addNode("24");
        graph.addNode("25");
        graph.addNode("26");
        graph.addNode("27");
        graph.addNode("28");
        graph.addNode("29");
        graph.addNode("30");

        graph.addEdge("root", "1");
        graph.addEdge("root", "2");
        graph.addEdge("root", "3");
        graph.addEdge("root", "4");
        graph.addEdge("root", "5");

        graph.addEdge("1", "6");
        graph.addEdge("1", "7");
        graph.addEdge("2", "8");
        graph.addEdge("2", "9");
        graph.addEdge("3", "10");
        graph.addEdge("3", "11");
        graph.addEdge("4", "12");
        graph.addEdge("4", "13");
        graph.addEdge("5", "14");
        graph.addEdge("5", "15");
        graph.addEdge("6", "16");
        graph.addEdge("6", "17");
        graph.addEdge("7", "18");
        graph.addEdge("7", "19");
        graph.addEdge("8", "20");
        graph.addEdge("8", "21");
        graph.addEdge("9", "22");
        graph.addEdge("9", "23");


        graph.addEdge("1", "24");
        graph.addEdge("3", "24");

        graph.addEdge("17", "25");
        graph.addEdge("18", "25");

        graph.addEdge("4", "5");

        graph.addEdge("14", "22");

        graph.addEdge("11", "26");
        graph.addEdge("11", "27");
        graph.addEdge("12", "28");
        graph.addEdge("12", "29");
        graph.addEdge("28", "30");
        graph.addEdge("29", "30");



    }
}
