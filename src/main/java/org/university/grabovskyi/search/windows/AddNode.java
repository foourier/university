package org.university.grabovskyi.search.windows;

import org.university.grabovskyi.search.GraphVisualization;

import javax.swing.*;
import java.awt.event.*;

public class AddNode extends JFrame {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField nodeName;

    public AddNode(GraphVisualization graph) {
        super("Add Node");
        setContentPane(contentPane);
        pack();
        setVisible(true);
        buttonOK.addActionListener(e -> addNode(graph));


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void addNode(GraphVisualization graph) {
        graph.addNode(nodeName.getText());
        onCancel();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
