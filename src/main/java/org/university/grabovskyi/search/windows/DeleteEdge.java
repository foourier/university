package org.university.grabovskyi.search.windows;

import org.graphstream.graph.Edge;
import org.university.grabovskyi.search.GraphVisualization;

import javax.swing.*;
import java.awt.event.*;

public class DeleteEdge extends JFrame {
    private JPanel contentPane;
    private JButton deleteButton;
    private JComboBox edgeName;

    public DeleteEdge(GraphVisualization graph) {
        super("Delete Edge");
        setContentPane(contentPane);

        setVisible(true);
        deleteButton.addActionListener(e -> deleteEdge(graph));
        for (Edge n :graph.getGraph().getEachEdge()){
            edgeName.addItem(n.getId());
        }
        pack();
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void deleteEdge(GraphVisualization graph) {
        graph.deleteEdge(edgeName.getSelectedItem().toString());
        onCancel();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
