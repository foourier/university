package org.university.grabovskyi.search.windows;

import org.graphstream.graph.Node;
import org.university.grabovskyi.search.GraphVisualization;

import javax.swing.*;
import java.awt.event.*;

public class DeleteNode extends JFrame {
    private JPanel contentPane;
    private JButton deleteButton;
    private JComboBox nodeName;

    public DeleteNode(GraphVisualization graph) {
        super("Delete Node");
        setContentPane(contentPane);

        setVisible(true);
        deleteButton.addActionListener(e -> deleteNode(graph));
        for (Node n :graph.getGraph().getEachNode()){
            nodeName.addItem(n.getId());
        }
        pack();
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void deleteNode(GraphVisualization graph) {
        graph.deleteNode(nodeName.getSelectedItem().toString());
        onCancel();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
