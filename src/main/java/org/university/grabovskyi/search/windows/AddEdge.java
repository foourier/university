package org.university.grabovskyi.search.windows;

import org.graphstream.graph.Node;
import org.university.grabovskyi.search.GraphVisualization;

import javax.swing.*;
import java.awt.event.*;

public class AddEdge extends JFrame {
    private JPanel contentPane;
    private JButton buttonOK;
    private JComboBox fromNode;
    private JComboBox toNode;

    public AddEdge(GraphVisualization graph) {
        super("Add Edge");
        setContentPane(contentPane);

        for (Node n :graph.getGraph().getEachNode()){
            fromNode.addItem(n.getId());
            toNode.addItem(n.getId());
        }

        pack();
        setVisible(true);


        buttonOK.addActionListener(e -> addEdge(graph));

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void addEdge(GraphVisualization graph) {
        graph.addEdge(fromNode.getSelectedItem().toString(), toNode.getSelectedItem().toString());
        onCancel();
    }

    private void onCancel() {
        dispose();
    }

}
