package org.university.grabovskyi.search.windows;

import org.graphstream.graph.Graph;

import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class ResultInfo extends JFrame {
    private JPanel contentPane;
    private JTextArea textArea1;
    private JTextArea textArea2;

    public ResultInfo(Stack<Integer> r, int[]labels, Graph g) {
        super("Route info");
        setContentPane(contentPane);

        List<Integer> route = new ArrayList<>();

        for(int x: r)
            route.add(0, x);

        for(int i=1; i<=labels.length; i++){
            for(int j=0; j<labels.length; j++){
                if(labels[j] == i){
                    textArea2.append("Number: " + i + " Node: " + g.getNode(j).getId() + "\n");
                }
            }
        }

        StringJoiner stringJoiner = new StringJoiner(" -> ");
        Collections.reverse(route);
        for (int x: route)
            stringJoiner.add(g.getNode(x).getId());

        textArea1.append(stringJoiner.toString());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);



        pack();
        setVisible(true);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
