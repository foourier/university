package org.university.grabovskyi.lee_wave;

public enum Direction {
    UP_LEFT,
    DIAGONAL,
    COMBINATION,
    UP_LEFT_BI_D,
    DIAGONAL_BI_D,
    COMBINATION_BI_D
}
