package org.university.grabovskyi.lee_wave.finder.two_road;

import org.university.grabovskyi.lee_wave.finder.LeeWaveFinder;
import org.university.grabovskyi.lee_wave.finder.Point;

import java.util.Deque;
import java.util.LinkedList;

public class FinderForUpLeftTwoRoad extends LeeWaveFinderForTwoRoadAbstract implements LeeWaveFinder {

    public FinderForUpLeftTwoRoad(int[][] adjacencyMatrix, int startRow, int finishRow, int startColumn, int finishColumn) {
        super(adjacencyMatrix, startRow, finishRow, startColumn, finishColumn);
    }

    @Override
    protected boolean openFirstRoad(Point p) {
        if(p.row-1 >= 0 && adjacencyMatrix[p.row-1][p.column] == 0){
            if(secondOpenList.contains(new Point(p.row-1, p.column, p.label+1))
//                    || secondCloseList.contains(new Point(p.row-1, p.column, p.label+1))
                    ){
                firstPoint = p;
                secondPoint = new Point(p.row-1, p.column);
                return true;
            }
            if(resultMatrix[p.row-1][p.column] == 0){
                resultMatrix[p.row-1][p.column] = p.label+1;
                firstCloseList.addLast(new Point(p.row-1, p.column, p.label+1));
            }
        }

        if(p.row+1 < adjacencyMatrix.length && adjacencyMatrix[p.row+1][p.column] == 0){
            if(secondOpenList.contains(new Point(p.row+1, p.column, p.label+1))
//                    || secondCloseList.contains(new Point(p.row+1, p.column, p.label+1))
                    ){
                firstPoint = p;
                secondPoint = new Point(p.row+1, p.column);
                return true;
            }
            if(resultMatrix[p.row+1][p.column] == 0){
                resultMatrix[p.row+1][p.column] = p.label+1;
                firstCloseList.addLast(new Point(p.row+1, p.column, p.label+1));
            }
        }

        if(p.column-1 >= 0 && adjacencyMatrix[p.row][p.column-1] == 0){
            if(secondOpenList.contains(new Point(p.row, p.column-1, p.label+1))
//                    || secondCloseList.contains(new Point(p.row, p.column-1, p.label+1))
                    ){
                firstPoint = p;
                secondPoint = new Point(p.row, p.column-1);
                return true;
            }
            if(resultMatrix[p.row][p.column-1] == 0){
                resultMatrix[p.row][p.column-1] = p.label+1;
                firstCloseList.addLast(new Point(p.row, p.column-1, p.label+1));
            }
        }

        if(p.column+1 < adjacencyMatrix[0].length && adjacencyMatrix[p.row][p.column+1] == 0){
            if(secondOpenList.contains(new Point(p.row, p.column+1, p.label+1))
//                    || secondCloseList.contains(new Point(p.row, p.column+1, p.label+1))
                    ){
                firstPoint = p;
                secondPoint = new Point(p.row, p.column+1);
                return true;
            }
            if(resultMatrix[p.row][p.column+1] == 0){
                resultMatrix[p.row][p.column+1] = p.label+1;
                firstCloseList.addLast(new Point(p.row, p.column+1, p.label+1));
            }
        }

        firstOpenList.add(p);
        return false;
    }

    @Override
    protected boolean openSecondRoad(Point p) {
        if(p.row-1 >= 0 && adjacencyMatrix[p.row-1][p.column] == 0){
            if(firstOpenList.contains(new Point(p.row-1, p.column, p.label+1))
//                    || firstCloseList.contains(new Point(p.row-1, p.column, p.label+1))
                    ){
                firstPoint = new Point(p.row-1, p.column);
                secondPoint = p;
                return true;
            }
            if(resultMatrix[p.row-1][p.column] == 0){
                resultMatrix[p.row-1][p.column] = p.label+1;
                secondCloseList.addLast(new Point(p.row-1, p.column, p.label+1));
            }
        }

        if(p.row+1 < adjacencyMatrix.length && adjacencyMatrix[p.row+1][p.column] == 0){
            if(firstOpenList.contains(new Point(p.row+1, p.column, p.label+1))
//                    || firstCloseList.contains(new Point(p.row+1, p.column, p.label+1))
                    ){
                firstPoint = new Point(p.row+1, p.column);
                secondPoint = p;
                return true;
            }
            if(resultMatrix[p.row+1][p.column] == 0){
                resultMatrix[p.row+1][p.column] = p.label+1;
                secondCloseList.addLast(new Point(p.row+1, p.column, p.label+1));
            }
        }

        if(p.column-1 >= 0 && adjacencyMatrix[p.row][p.column-1] == 0){
            if(firstOpenList.contains(new Point(p.row, p.column-1, p.label+1))
//                    || firstCloseList.contains(new Point(p.row, p.column-1, p.label+1))
                    ){
                firstPoint = new Point(p.row, p.column-1);
                secondPoint = p;
                return true;
            }
            if(resultMatrix[p.row][p.column-1] == 0){
                resultMatrix[p.row][p.column-1] = p.label+1;
                secondCloseList.addLast(new Point(p.row, p.column-1, p.label+1));
            }
        }

        if(p.column+1 < adjacencyMatrix[0].length && adjacencyMatrix[p.row][p.column+1] == 0){
            if(firstOpenList.contains(new Point(p.row, p.column+1, p.label+1))
//                    || firstCloseList.contains(new Point(p.row, p.column+1, p.label+1))
                    ){
                firstPoint = new Point(p.row, p.column+1);
                secondPoint = p;
                return true;
            }
            if(resultMatrix[p.row][p.column+1] == 0){
                resultMatrix[p.row][p.column+1] = p.label+1;
                secondCloseList.addLast(new Point(p.row, p.column+1, p.label+1));
            }
        }
        secondOpenList.add(p);
        return false;
    }


    @Override
    public Deque<Point> getRoute() {
        Deque<Point> route = new LinkedList<>();
        Point currentPoint = firstPoint;
        route.addFirst(currentPoint);

        while (resultMatrix[currentPoint.row][currentPoint.column] != 1){
            if(currentPoint.row-1 >= 0
                    && resultMatrix[currentPoint.row-1][currentPoint.column]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row-1, currentPoint.column, resultMatrix[currentPoint.row-1][currentPoint.column]);
                route.addFirst(currentPoint);
            }else if(currentPoint.row+1 < resultMatrix.length
                    && resultMatrix[currentPoint.row+1][currentPoint.column]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row+1, currentPoint.column, resultMatrix[currentPoint.row+1][currentPoint.column]);
                route.addFirst(currentPoint);
            }else if(currentPoint.column-1 >= 0
                    && resultMatrix[currentPoint.row][currentPoint.column-1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row, currentPoint.column-1, resultMatrix[currentPoint.row][currentPoint.column-1]);
                route.addFirst(currentPoint);
            }else if(currentPoint.column+1 < resultMatrix[0].length
                    && resultMatrix[currentPoint.row][currentPoint.column+1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row, currentPoint.column+1, resultMatrix[currentPoint.row][currentPoint.column+1]);
                route.addFirst(currentPoint);
            }
        }

        currentPoint = secondPoint;
        route.addLast(currentPoint);

        while (resultMatrix[currentPoint.row][currentPoint.column] != 1){
            if(currentPoint.row-1 >= 0
                    && resultMatrix[currentPoint.row-1][currentPoint.column]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row-1, currentPoint.column, resultMatrix[currentPoint.row-1][currentPoint.column]);
                route.addLast(currentPoint);
            }else if(currentPoint.row+1 < resultMatrix.length
                    && resultMatrix[currentPoint.row+1][currentPoint.column]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row+1, currentPoint.column, resultMatrix[currentPoint.row+1][currentPoint.column]);
                route.addLast(currentPoint);
            }else if(currentPoint.column-1 >= 0
                    && resultMatrix[currentPoint.row][currentPoint.column-1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row, currentPoint.column-1, resultMatrix[currentPoint.row][currentPoint.column-1]);
                route.addLast(currentPoint);
            }else if(currentPoint.column+1 < resultMatrix[0].length
                    && resultMatrix[currentPoint.row][currentPoint.column+1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row, currentPoint.column+1, resultMatrix[currentPoint.row][currentPoint.column+1]);
                route.addLast(currentPoint);
            }
        }

        return route;
    }
}
