package org.university.grabovskyi.lee_wave.finder;

import java.util.Deque;
import java.util.LinkedList;

public class FinderForDiagonal extends LeeWaveFinderAbstract implements LeeWaveFinder {
    public FinderForDiagonal(int[][] adjacencyMatrix, int startRow, int finishRow, int startColumn, int finishColumn) {
        super(adjacencyMatrix, startRow, finishRow, startColumn, finishColumn);
    }

    @Override
    protected boolean openNode(Deque<Point> closeList, Point findPoint) {
        Point p = closeList.removeFirst();
        if(p.row-1 >= 0 && p.column-1 >= 0 && adjacencyMatrix[p.row-1][p.column-1] == 0){
            if(findPoint.equals(new Point(p.row-1, p.column-1))){
                resultMatrix[p.row-1][p.column-1] = p.label+1;
                closeList.addLast(new Point(p.row-1, p.column-1, p.label+1));
                openList.add(p);
                return true;
            }
            if(resultMatrix[p.row-1][p.column-1] == 0){
                resultMatrix[p.row-1][p.column-1] = p.label+1;
                closeList.addLast(new Point(p.row-1, p.column-1, p.label+1));
            }
        }
        if(p.row+1 < adjacencyMatrix.length && p.column-1 >= 0 && adjacencyMatrix[p.row+1][p.column-1] == 0){
            if(findPoint.equals(new Point(p.row+1, p.column-1))){
                resultMatrix[p.row+1][p.column-1] = p.label+1;
                closeList.addLast(new Point(p.row+1, p.column-1, p.label+1));
                openList.add(p);
                return true;
            }
            if(resultMatrix[p.row+1][p.column-1] == 0){
                resultMatrix[p.row+1][p.column-1] = p.label+1;
                closeList.addLast(new Point(p.row+1, p.column-1, p.label+1));
            }
        }
        if(p.row-1 >= 0 && p.column+1 < adjacencyMatrix[0].length && adjacencyMatrix[p.row-1][p.column+1] == 0){
            if(findPoint.equals(new Point(p.row-1, p.column+1))){
                resultMatrix[p.row-1][p.column-1] = p.label+1;
                closeList.addLast(new Point(p.row-1, p.column+1, p.label+1));
                openList.add(p);
                return true;
            }
            if(resultMatrix[p.row-1][p.column+1] == 0){
                resultMatrix[p.row-1][p.column+1] = p.label+1;
                closeList.addLast(new Point(p.row-1, p.column+1, p.label+1));
            }
        }
        if(p.row+1 < adjacencyMatrix.length && p.column+1 < adjacencyMatrix[0].length && adjacencyMatrix[p.row+1][p.column+1] == 0){
            if(findPoint.equals(new Point(p.row+1, p.column+1))){
                resultMatrix[p.row+1][p.column+1] = p.label+1;
                closeList.addLast(new Point(p.row+1, p.column+1, p.label+1));
                openList.add(p);
                return true;
            }
            if(resultMatrix[p.row+1][p.column+1] == 0){
                resultMatrix[p.row+1][p.column+1] = p.label+1;
                closeList.addLast(new Point(p.row+1, p.column+1, p.label+1));
            }
        }
        openList.add(p);
        return false;
    }

    @Override
    public Deque<Point> getRoute() {
        Deque<Point> route = new LinkedList<>();
        Point currentPoint = new Point(finishRow, finishColumn, resultMatrix[finishRow][finishColumn]);
        route.addFirst(currentPoint);

        while (resultMatrix[currentPoint.row][currentPoint.column] != 1){
            if(currentPoint.row-1 >= 0 && currentPoint.column-1 >= 0
                    && resultMatrix[currentPoint.row-1][currentPoint.column-1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row-1, currentPoint.column-1, resultMatrix[currentPoint.row-1][currentPoint.column-1]);
                route.addFirst(currentPoint);
            }else if(currentPoint.row+1 < resultMatrix.length && currentPoint.column-1 >= 0
                    && resultMatrix[currentPoint.row+1][currentPoint.column-1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row+1, currentPoint.column-1, resultMatrix[currentPoint.row+1][currentPoint.column-1]);
                route.addFirst(currentPoint);
            }else if(currentPoint.row-1 >= 0 && currentPoint.column+1 < resultMatrix[0].length
                    && resultMatrix[currentPoint.row-1][currentPoint.column+1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row-1, currentPoint.column+1, resultMatrix[currentPoint.row-1][currentPoint.column+1]);
                route.addFirst(currentPoint);
            }else if(currentPoint.row+1 < resultMatrix.length && currentPoint.column+1 < resultMatrix[0].length
                    && resultMatrix[currentPoint.row+1][currentPoint.column+1]
                    == resultMatrix[currentPoint.row][currentPoint.column]-1){
                currentPoint = new Point(currentPoint.row+1, currentPoint.column+1, resultMatrix[currentPoint.row+1][currentPoint.column+1]);
                route.addFirst(currentPoint);
            }
        }
        return route;
    }

}
