package org.university.grabovskyi.lee_wave.finder.two_road;

import lombok.Getter;
import org.university.grabovskyi.lee_wave.finder.LeeWaveFinder;
import org.university.grabovskyi.lee_wave.finder.Point;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public abstract class LeeWaveFinderForTwoRoadAbstract implements LeeWaveFinder {

    protected int[][] adjacencyMatrix;

    protected int[][] resultMatrix;
    protected int startRow, finishRow;
    protected int startColumn, finishColumn;
    protected List<Point> firstOpenList;
    protected List<Point> secondOpenList;
    protected Deque<Point> firstCloseList;
    protected Deque<Point> secondCloseList;
    protected Point firstPoint;
    protected Point secondPoint;

    public LeeWaveFinderForTwoRoadAbstract(int[][] adjacencyMatrix,
                                 int startRow,
                                 int finishRow,
                                 int startColumn,
                                 int finishColumn) {
        this.adjacencyMatrix = adjacencyMatrix;
        this.startRow = startRow;
        this.finishRow = finishRow;
        this.startColumn = startColumn;
        this.finishColumn = finishColumn;
        this.firstCloseList = new LinkedList<>();
        this.secondCloseList = new LinkedList<>();
        resultMatrix = new int[adjacencyMatrix.length][adjacencyMatrix[0].length];
    }


    public boolean search(){
        firstOpenList = new ArrayList<>();
        secondOpenList = new ArrayList<>();
        Point startPoint  = new Point(startRow, startColumn, 1);
        Point findPoint  = new Point(finishRow, finishColumn, 1);

        firstCloseList.addFirst(startPoint);
        secondCloseList.addFirst(findPoint);

        resultMatrix[startRow][startColumn] = 1;
        resultMatrix[finishColumn][finishColumn] = 1;

        while (!firstCloseList.isEmpty() || !secondCloseList.isEmpty() ){
            if(openNode())
                return true;
        }
        return false;
    }


    protected abstract boolean openFirstRoad(Point p);
    protected abstract boolean openSecondRoad(Point p);


    protected boolean openNode(){

        // знайдено перетин
        if (!firstCloseList.isEmpty() && openFirstRoad(firstCloseList.removeFirst()))
            return true;

        // знайдено перетин
        if (!secondCloseList.isEmpty() && openSecondRoad(secondCloseList.removeFirst()))
            return true;

        // перетину не знайдено
        return false;
    }

    @Override
    public void show(){

        for (int[] i : adjacencyMatrix){
            for (int j : i){
                if(j == -1)
                    System.out.print("*\t");
                else
                    System.out.print(""+j+"\t");
            }
            System.out.println();
        }
        System.out.println("----------------------------------------------");
        for (int[] i : resultMatrix){
            for (int j : i){
                System.out.print(""+j + "\t");
            }
            System.out.println();
        }
    }

    @Override
    public int[][] getResultMatrix() {
        return resultMatrix;
    }
}
