package org.university.grabovskyi.lee_wave.finder;

import lombok.Getter;

import java.util.*;

public abstract class LeeWaveFinderAbstract implements LeeWaveFinder {

    protected int[][] adjacencyMatrix;
    protected int[][] resultMatrix;
    protected int startRow, finishRow;
    protected int startColumn, finishColumn;
    protected List<Point> openList;

    public LeeWaveFinderAbstract(int[][] adjacencyMatrix,
                                 int startRow,
                                 int finishRow,
                                 int startColumn,
                                 int finishColumn) {
        this.adjacencyMatrix = adjacencyMatrix;
        this.startRow = startRow;
        this.finishRow = finishRow;
        this.startColumn = startColumn;
        this.finishColumn = finishColumn;
        resultMatrix = new int[adjacencyMatrix.length][adjacencyMatrix[0].length];
    }


    public boolean search(){
        Deque<Point> closeList = new LinkedList<>();
        openList = new ArrayList<>();
        Point findPoint  = new Point(finishRow, finishColumn);

        closeList.addLast(new Point(startRow, startColumn, 1));
        resultMatrix[startRow][startColumn] = 1;

        while (!closeList.isEmpty()){
            if(openNode(closeList, findPoint))
                return true;
        }
        return false;
    }

    protected abstract boolean openNode(Deque<Point> closeList, Point findPoint);

    @Override
    public void show(){

        for (int[] i : adjacencyMatrix){
            for (int j : i){
                if(j == -1)
                    System.out.print("*\t");
                else
                    System.out.print(""+j+"\t");
            }
            System.out.println();
        }
        System.out.println("----------------------------------------------");
        for (int[] i : resultMatrix){
            for (int j : i){
                System.out.print(""+j + "\t");
            }
            System.out.println();
        }
    }

    @Override
    public int[][] getResultMatrix() {
        return resultMatrix;
    }
}
