package org.university.grabovskyi.lee_wave.finder;

import java.util.Deque;

public interface LeeWaveFinder {
    boolean search();
    void show();
    Deque<Point> getRoute();
    int[][] getResultMatrix();
}
