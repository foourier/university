package org.university.grabovskyi.lee_wave.finder;

import java.util.Objects;

public class Point {
        public int row;
        public int column;

        public int label;

        public Point(int row, int column) {
            this.row = row;
            this.column = column;
            label = 0;
        }

        public Point(int row, int column, int label) {
            this.row = row;
            this.column = column;
            this.label = label;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            org.university.grabovskyi.lee_wave.finder.Point point = (org.university.grabovskyi.lee_wave.finder.Point) o;
            return row == point.row &&
                    column == point.column;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, column);
        }

        @Override
        public String toString() {
            return "Step: " + label + " row: " + (row+1) + " column: " + (column+1) +"\n";
        }
    }

