package org.university.grabovskyi.lee_wave;

import org.university.grabovskyi.lee_wave.windows.Grid;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(Grid::new);
    }
}
