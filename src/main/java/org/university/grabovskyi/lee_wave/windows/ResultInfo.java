package org.university.grabovskyi.lee_wave.windows;

import org.university.grabovskyi.lee_wave.finder.Point;

import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class ResultInfo extends JFrame {
    private JPanel contentPane;
    private JTextArea textArea1;
    private JTextField time;

    public ResultInfo(Deque<Point> route, long time) {
        super("Route info");
        setContentPane(contentPane);


        for(Point p: route)
            textArea1.append(p.toString());

        this.time.setText("Time of execution: " + String.format("%,12d",time) + " ns");



        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);



        pack();
        setVisible(true);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
