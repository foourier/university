package org.university.grabovskyi.lee_wave.windows;

import org.university.grabovskyi.lee_wave.*;
import org.university.grabovskyi.lee_wave.finder.*;
import org.university.grabovskyi.lee_wave.finder.Point;
import org.university.grabovskyi.lee_wave.finder.two_road.FinderForCombinationTwoRoad;
import org.university.grabovskyi.lee_wave.finder.two_road.FinderForDiagonalTwoRoad;
import org.university.grabovskyi.lee_wave.finder.two_road.FinderForUpLeftTwoRoad;
import org.university.grabovskyi.search.windows.ErrorWindow;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
//import java.awt.*;
import java.awt.*;
import java.util.Deque;

public class Grid extends JFrame {
    private JPanel contentPane;
    private JTable grid;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JComboBox comboBox1;
    private JButton searchBtn;
    private JButton clear;
    private JButton add;
    private int rowCount;
    private int columnCount;


    int[][] startMatrix = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},
            {0, 0, -1, 0, 0, -1, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0},
            {0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0},
            {0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0},
            {0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0},
            {0, 0, -1, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, -1, -1, -1, 0, 0, 0, 0, 0, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, -1, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    };

    public Grid() {
        super("Lee wave finder");
        setContentPane(contentPane);
        init();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        searchBtn.addActionListener(e -> search());
        clear.addActionListener(e -> clear());
        add.addActionListener(e -> addColumnAndRow());

        for(Direction d: Direction.values())
            comboBox1.addItem(d);

        pack();
        setVisible(true);
    }

    private void search() {
        startMatrix = new int[rowCount][columnCount];
        boolean flag = false;
        try {
            for (int i = 0; i < rowCount; i++) {
                for (int j = 0; j < columnCount; j++) {
                    startMatrix[i][j] = Integer.parseInt(grid.getValueAt(i, j).toString());
                    if (startMatrix[i][j] != 0 && startMatrix[i][j] != -1)
                        flag = true;
                }
            }
        }catch (Exception ex){
            new ErrorWindow("Некоректний ввід!!!");
        }

        if (flag) {
            new ErrorWindow("Некоректний ввід!!!");
            return;
        }
        int startRow = Integer.parseInt(textField1.getText());
        if(startRow <= 0 || startRow > rowCount){
            new ErrorWindow("Некоректний ввід!!1");
            return;
        }

        int startColumn = Integer.parseInt(textField2.getText());
        if(startColumn <= 0 || startColumn > columnCount){
            new ErrorWindow("Некоректний ввід!!1");
            return;
        }
        int finishRow = Integer.parseInt(textField3.getText());
        if(finishRow <= 0 || finishRow > rowCount){
            new ErrorWindow("Некоректний ввід!!1");
            return;
        }
        int finishColumn = Integer.parseInt(textField4.getText());
        if(finishColumn <= 0 || finishColumn > columnCount){
            new ErrorWindow("Некоректний ввід!!1");
            return;
        }
        LeeWaveFinder leeWaveFinder = null;
        if( Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.UP_LEFT ) {
            leeWaveFinder = new FinderForUpLeft(startMatrix,
                    startRow - 1,
                    finishRow - 1,
                    startColumn - 1,
                    finishColumn - 1);
        } else if (Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.DIAGONAL ){
            leeWaveFinder = new FinderForDiagonal(startMatrix,
                    startRow - 1,
                    finishRow - 1,
                    startColumn - 1,
                    finishColumn - 1);
        } else if (Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.COMBINATION ){
            leeWaveFinder = new FinderForCombination(startMatrix,
                    startRow - 1,
                    finishRow - 1,
                    startColumn - 1,
                    finishColumn - 1);
        }else if (Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.UP_LEFT_BI_D ){
            leeWaveFinder = new FinderForUpLeftTwoRoad(startMatrix,
                    startRow - 1,
                    finishRow - 1,
                    startColumn - 1,
                    finishColumn - 1);
        }else if (Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.DIAGONAL_BI_D ){
            leeWaveFinder = new FinderForDiagonalTwoRoad(startMatrix,
                    startRow - 1,
                    finishRow - 1,
                    startColumn - 1,
                    finishColumn - 1);
        }else{
            leeWaveFinder = new FinderForCombinationTwoRoad(startMatrix,
                    startRow - 1,
                    finishRow - 1,
                    startColumn - 1,
                    finishColumn - 1);
        }

        long t = System.nanoTime();
        leeWaveFinder.search();
        long t2 = System.nanoTime();
        Deque<Point> route = leeWaveFinder.getRoute();

        setGrid(leeWaveFinder.getResultMatrix());
        grid.setDefaultRenderer(Object.class, new Renderer(route));
        new ResultInfo(route, getTime(t, t2) );
    }

    private void setGrid(int [][] matrix){
        for(int i=0; i<rowCount; i++)
            for(int j=0; j<columnCount; j++)
                grid.setValueAt(matrix[i][j], i, j);
    }

    private void init(){
        DefaultTableModel newModel = (DefaultTableModel) grid.getModel();
        rowCount = 30;
        columnCount = 30;
        newModel.setColumnCount(columnCount);
        newModel.setRowCount(rowCount);
        setGrid(startMatrix);
        grid.setDefaultRenderer(Object.class, new DefaultRenderer());
    }

    private void addColumnAndRow(){
        DefaultTableModel newModel = (DefaultTableModel) grid.getModel();
        rowCount++;
        columnCount++;
        newModel.setColumnCount(columnCount);
        newModel.setRowCount(rowCount);

        for(int i=0; i<rowCount; i++)
            grid.setValueAt(0, i, columnCount-1);
        for(int j=0; j<columnCount; j++)
            grid.setValueAt(0, rowCount-1, j);

        startMatrix = new int[rowCount][columnCount];

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                startMatrix[i][j] = Integer.parseInt(grid.getValueAt(i, j).toString());
            }
        }
        pack();
    }

    private void clear(){
        setGrid(startMatrix);
        grid.setDefaultRenderer(Object.class, new DefaultRenderer());
    }

    public class Renderer extends DefaultTableCellRenderer {
        Deque<Point> route;
        public Renderer(Deque<Point> route) {
            this.route = route;
        }
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            Component cell = super.getTableCellRendererComponent(table, value, isSelected, true, row, column);
            for (Point p: route){
                if(row == p.row && column == p.column) {
                    cell.setBackground(Color.RED);
                }
            }
            if(startMatrix[row][column] == -1)
                cell.setBackground(Color.BLACK);
            return cell;
        }
    }

    public class DefaultRenderer extends DefaultTableCellRenderer {
        public DefaultRenderer() {}
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            Component cell = super.getTableCellRendererComponent(table, value, isSelected, true, row, column);
            cell.setBackground(Color.WHITE);
            if(value.toString().equals("-1"))
                cell.setBackground(Color.BLACK);
            return cell;
        }
    }


    private long getTime(long t1, long t2){
        if( Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.UP_LEFT_BI_D
                || Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.DIAGONAL_BI_D
                || Direction.valueOf( comboBox1.getSelectedItem().toString() ) == Direction.COMBINATION_BI_D){
            return (t2-t1)/16;
        }
        return t2-t1;
    }
}
