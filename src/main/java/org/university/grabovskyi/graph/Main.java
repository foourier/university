package org.university.grabovskyi.graph;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import java.awt.*;

public class Main {

    private static String styleSheet =
            "node {" +

                    "size: 3px;\n" +
                    "\tfill-color: #777;\n" +
                    "\ttext-mode: hidden;\n" +
                    "\tz-index: 0;" +
                    "}" +
                    "edge {\n" +
                    "\tshape: line;\n" +
                    "\tfill-color: #222;\n" +
                    "\tarrow-size: 3px, 2px;\n" +
                    "}" +
                    "edge.tollway { size: 2px; stroke-color: red; stroke-width: 1px; stroke-mode: plain; }\n" +
                    "edge.tunnel { stroke-color: blue; stroke-width: 1px; stroke-mode: plain; }\n" +
                    "edge.bridge { stroke-color: yellow; stroke-width: 1px; stroke-mode: plain; }" +
                    "graph {" +
                    //"fill-mode: image-scaled;" +
                    //"fill-image: url('\\src\\main\\resources\\grabovskiy\\leHavre.png');" +
                    "fill-mode: image-scaled-ratio-max; " +
                    "fill-image: url('D:\\git\\university\\src\\main\\resources\\grabovskiy\\leHavre.png');  " +
                    "}";

    public static void main(String[] args) {
        //System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        Graph graph = new MultiGraph("Le Havre");

        try {
            graph.read("src\\main\\resources\\grabovskiy\\LeHavre.dgs");
        } catch(Exception e) {
            e.printStackTrace();
        }


        for(Edge edge: graph.getEachEdge()) {
            if(edge.hasAttribute("isTollway")) {
                edge.addAttribute("ui.class", "tollway");
            } else if(edge.hasAttribute("isTunnel")) {
                edge.addAttribute("ui.class", "tunnel");
            } else if(edge.hasAttribute("isBridge")) {
                edge.addAttribute("ui.class", "bridge");
            }
        }


        graph.addAttribute("ui.stylesheet", styleSheet);
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
//        graph.addAttribute("ui.screenshot", "src\\main\\resources\\grabovskiy\\leHavre.png");
        Viewer viewer = graph.display(false);
//        view.setViewCenter(440000, 2503000, 0);
//        view.setViewPercent(0.25);
    }
}
