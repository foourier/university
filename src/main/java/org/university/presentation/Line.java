package org.university.presentation;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import java.util.List;

public class Line extends ApplicationFrame {
    protected List<Double> x;
    protected List<Double> y;
    protected List<List<Double>> data;


    public Line(String title, List<Double> x, List<Double> y) {
        super(title);
        this.x = x;
        this.y = y;
    }

    public Line(String title, List<Double> x, List<List<Double>> y, boolean d) {
        super(title);
        this.x = x;
        data = y;
    }

    public void LineChart_AWT() {
        JFreeChart lineChart = ChartFactory.createXYLineChart(
                getTitle(),
                "X",
                "F",
                createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
        setContentPane(chartPanel);
    }

    private XYDataset createDataset() {
        final XYSeriesCollection data = new XYSeriesCollection();
        if (this.data == null) {
            final XYSeries dataset =
                    new XYSeries(getTitle());
            for (int i = 0; i < x.size(); i++) {
                dataset.add(x.get(i), y.get(i));
            }
            data.addSeries(dataset);
            return data;
        } else {
            for (List<Double> Y : this.data) {
                XYSeries dataset =
                        new XYSeries(getTitle());
                for (int i = 0; i < x.size(); i++) {
                    dataset.add(x.get(i), Y.get(i));
                }
                data.addSeries(dataset);
            }
            return data;
        }
    }
}
