package org.university.presentation;

import org.jfree.ui.RefineryUtilities;

import java.util.List;

public class ShowPoint {
    public static void show(String title , List<Double> x, List<Double> y){
        Point p = new Point( title, x, y);
        p.LineChart_AWT();
        p.pack();
        RefineryUtilities.centerFrameOnScreen(p);
        p.setVisible(true);
    }
}
