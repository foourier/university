package org.university.presentation;

import org.jfree.ui.RefineryUtilities;

import java.util.List;

public class ShowGistagram {
    public static void show(String title , List<Double> x, List<Double> y){
        Gistagram g = new Gistagram( title, x, y);
        g.BarChart_AWT();
        g.pack();
        RefineryUtilities.centerFrameOnScreen(g);
        g.setVisible(true);
    }
}
