package org.university.presentation;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

import java.util.List;

public class Gistagram extends ApplicationFrame {


    protected List<Double> x;
    protected List<Double> y;

    public Gistagram(String title, List<Double> x, List<Double> y) {
        super(title);
        this.x = x;
        this.y = y;
    }

    public void BarChart_AWT(){
        JFreeChart barChart = ChartFactory.createBarChart(
                getTitle(),
                "X",
                "F",
                createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel( barChart );
        chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );
        setContentPane( chartPanel );
    }

    private CategoryDataset createDataset(){
        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset( );

        for(int i=0; i<x.size(); i++){
            dataset.addValue(x.get(i), y.get(i), y.get(i));
        }

        return dataset;
    }

}
