package org.university.presentation;

import org.jfree.ui.RefineryUtilities;

import java.util.List;
import java.util.Map;

public class ShowLine {
    public static void show(String title, List<Double> x, List<Double> y) {
        Line l = new Line(title, x, y);
        l.LineChart_AWT();
        l.pack();
        RefineryUtilities.centerFrameOnScreen(l);
        l.setVisible(true);
    }

    public static void show(String title, List<Double> x, List<List<Double>> y, boolean b) {
        Line l = new Line(title, x, y, b);
        l.LineChart_AWT();
        l.pack();
        RefineryUtilities.centerFrameOnScreen(l);
        l.setVisible(true);
    }
}
