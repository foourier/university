package org.university.bolesta;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

     TextAnalizer analizer = new TextAnalizer();
     analizer.readText(new File("D:\\git\\university\\src\\main\\resources\\bolesta\\Chervone-i-chorne-Stendal.txt"));
     analizer.createData();
     analizer.filterResult(0.0001);
     analizer.printResult();
     analizer.printToFile();
    }
}
