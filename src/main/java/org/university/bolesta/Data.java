package org.university.bolesta;

public class Data {
    private String str;

    private long count;

    private double probability;

    public long getCount() {
        return count;
    }

    public String getStr() {
        return str;
    }

    public Data(String str) {
        this.str = str;
        this.count = 0;
        this.probability = 0.0;
    }

    public void addCount(long count) {
        this.count = count;
    }

    public void addCount() {
        count++;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(long totalCount) {
        this.probability = (double) count / (double) totalCount;
    }

    @Override
    public String toString() {
        return str + ":\t" + String.format("%d;\tP =\t%.4f", count, probability);
    }
}