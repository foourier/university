package org.university.bolesta;

import scala.util.parsing.combinator.testing.Str;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TextAnalizer {

    private static final String REGEX = "\\[|\\}|\\!|\\.|\\,|\\?|\\\"|\\'|\\;|\\:|\\(|\\)|\\d|\\]|\\{|\\—|\\`|\\«|\\»|\\-|\\’";

    private String book;
    private List<String> words;
    private List<String> combination;
    List<Data> data;

    private int countOfCombination;
    private long countOfUniqCombination;

    public TextAnalizer() {
        words = new ArrayList<>();
        combination = new ArrayList<>();
    }

    public void createData() {
        createCombination();
        countOfCombination = combination.size();
        countOfUniqCombination = combination.parallelStream().distinct().count();

        data = combination.parallelStream().distinct().map(Data::new).collect(Collectors.toList());

        for (Data d : data) {
            d.addCount(combination.parallelStream().filter(str -> str.equals(d.getStr())).count());
            d.setProbability(countOfCombination);
        }

        data = data.parallelStream().sorted((x, y) -> {
            int answer;
            if (x.getCount() == y.getCount()) {
                answer = 0;
            } else if (x.getCount() > y.getCount()) {
                answer = 1;
            } else {
                answer = -1;
            }
            return -1 * answer;
        }).collect(Collectors.toList());
    }

    public void filterResult(double minProbability) {
        data = data.parallelStream().filter(e -> e.getProbability() >= minProbability).collect(Collectors.toList());
    }

    private void createCombination() {
        for (String word : words) {
            char[] comb = word.toCharArray();
            for (int i = 1; i < comb.length; i++) {
                combination.add(word.substring(i - 1, i + 1));
            }
        }
    }

    public void readText(File file) {
        StringBuilder igText = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.lines().forEach(igText::append);
            String text = igText.toString().replaceAll(REGEX, "");
            words = Arrays.stream(text.split(" "))
                    .parallel()
                    .map(String::strip).map(String::toLowerCase).collect(Collectors.toList());
            System.out.println("Success");
        } catch (FileNotFoundException e) {
            System.out.println("Fail");
            e.printStackTrace();
        }
    }

    public void printResult(){
        System.out.println("Count of combination :\t" + countOfCombination);
        System.out.println("Count of uniq combination :\t" + countOfUniqCombination);
        System.out.println("Result of combination :");
        data.forEach(System.out::println);
    }

    public void printToFile(){
        File file = new File("src\\main\\resources\\bolesta\\result.txt");
        try (Writer writer = new FileWriter(file)) {
            for (Data d: data){
                writer.write(d.toString());
                writer.write('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
