package org.university.mochylskyi.space_features;

public class Main {
    public static void main(String[] args) {
        SpaceFeatures features = new SpaceFeatures(5, 10, 5);
        features.calcAll();
        features.print();
    }
}
