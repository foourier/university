package org.university.mochylskyi.space_features;

import java.util.Random;

public class SpaceFeatures {

    private double[][][] data;

    private double[][] classCenter;
    private double[][] Mx;
    private double[][] Dx;

    //Класи
    private int M;
    //Образи
    private int N;
    //Ознаки
    private int K;

    private double dispersOznak = 1;

    public SpaceFeatures(int m, int n, int k) {
        M = m;
        N = n;
        K = k;
        data = new double[M][N][K];
        Mx = new double[M][K];
        Dx = new double[M][K];
        classCenter = new double[M][K];

        classCenter[0][0] = 5;
        classCenter[0][1] = 1;
        classCenter[0][2] = 2;
        classCenter[0][3] = 3;
        classCenter[0][4] = 0;

        classCenter[1][0] = -2;
        classCenter[1][1] = 1;
        classCenter[1][2] = 2;
        classCenter[1][3] = 3;
        classCenter[1][4] = 1;

        classCenter[2][0] = 5;
        classCenter[2][1] = 6;
        classCenter[2][2] = 7;
        classCenter[2][3] = 8;
        classCenter[2][4] = 5;

        classCenter[3][0] = -5;
        classCenter[3][1] = -4;
        classCenter[3][2] = -3;
        classCenter[3][3] = -1;
        classCenter[3][4] = -5;

        classCenter[4][0] = 20;
        classCenter[4][1] = 20;
        classCenter[4][2] = 20;
        classCenter[4][3] = 20;
        classCenter[4][4] = 20;
    }

    private double NormRozp(double mx, double dx, Random rand) {
        double ksi_sh = 0.0;
        double sum_ks = 0.0;
        for (int j = 1; j <= 5; j++)
        {
            sum_ks += rand.nextDouble();
        }
        ksi_sh = Math.sqrt(2.4) * (sum_ks - 2.5);
        return mx + dx * (0.01 * ksi_sh * (97.0 + (ksi_sh * ksi_sh)));
    }

    private void fillObraz() {
        Random rand = new Random();
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                for (int l = 0; l < K; l++)
                    data[i][j][l] = NormRozp(classCenter[i][l], dispersOznak, rand);
    }

    private void calcMx(){
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
                for (int l = 0; l < K; l++)
                    Mx[i][l] += data[i][j][l];
        }
        for (int i = 0; i < M; i++)
            for (int j = 0; j < K; j++)
                Mx[i][j] /= N;
    }

    private  void getMx2(double[][] Mx){
        for (int i = 0; i < M; i++){
            for (int j = 0; j < N; j++)
                for (int l = 0; l < K; l++)
                    Mx[i][l] += data[i][j][l] * data[i][j][l];
        }
        for (int i = 0; i < M; i++)
            for (int j = 0; j < K; j++)
                Mx[i][j] /= N;
    }

    private void calcDx(){
        double[][] Mx2=new double [M][K];
        getMx2(Mx2);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < K; j++)
                Dx[i][j] = Mx2[i][j] - Mx[i][j] * Mx[i][j];
    }

    private double getDistance(int from, int to){
        double dist=0;
        for (int i = 0; i < K; i++)
            dist += (classCenter[to][i] - classCenter[from][i]) * (classCenter[to][i] - classCenter[from][i]);
        return Math.sqrt(dist);
    }

    public void calcAll(){
        fillObraz();
        calcMx();
        calcDx();
    }

    public void print(){
        for (int i = 0; i < M; i++){
            System.out.println("Class:" + i);
            for (int j = 0; j < N; j++){
                System.out.println("Obraz:" + j);
                for (int l = 0; l < K; l++){
                    System.out.print(data[i][j][l] + " ");
                }
                System.out.println();
            }
        }
        System.out.println("MX:");
        for (int i = 0; i < M; i++){
            System.out.println("Class:" + i);
            for (int j = 0; j < K; j++)
                System.out.print(Mx[i][j] + " ");
            System.out.println();
        }
        System.out.println("DISPERSION:");
        double tempDsum = 0;
        double []dispForClass= new double [M];
        for (int i = 0; i < M; i++){
            System.out.println("Class:" + i);
            for (int j = 0; j < K; j++){
                System.out.print(Dx[i][j] + " ");
                tempDsum += Dx[i][j];
            }
            dispForClass[i]=tempDsum/(double)K;
            System.out.println("\nDISPERSION FOR "+ i +" CLASS = " + dispForClass[i] );
            tempDsum = 0;
        }
        System.out.println("COMPARING ANALIST:");
        for (int i = 0; i < M; i++){
            for (int j=i;j<M;j++) {
                if (j != i) {
                    if (getDistance(i, j) >= 3 * dispForClass[i] + 3 * dispForClass[j])
                        System.out.print("dist between " + i + " and " + j + "=" + getDistance(i, j) + " >= " + (3 * dispForClass[i] + 3 * dispForClass[j]) + "(3*" + dispForClass[i] + "+3*" + dispForClass[j] + ") GOOD");
                    else
                        System.out.print("dist between " + i + " and " + j + "=" + getDistance(i, j) + " < " + (3 * dispForClass[i] + 3 * dispForClass[j]) + "(3*" + dispForClass[i] + "+3*" + dispForClass[j] + ") BAD");
                    System.out.println();
                }
            }
        }
    }

}
