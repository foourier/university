package org.university.mochylskyi.rozp;

import java.io.File;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Parser parser = new Parser(new File("src\\main\\resources\\moch\\1.png"));
        System.out.println(parser.parse());
    }
}
