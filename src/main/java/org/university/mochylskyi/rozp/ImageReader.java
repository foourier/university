package org.university.mochylskyi.rozp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ImageReader {
    public static int[][] read(File file){

        int [][]pixelMatrix = null;
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            int []pixels = bufferedImage.getRGB(0, 0 , bufferedImage.getWidth(), bufferedImage.getHeight(), null, 0, bufferedImage.getWidth() );
            pixelMatrix = new int[bufferedImage.getHeight()][bufferedImage.getWidth()];
            for(int i=0; i<bufferedImage.getHeight(); i++){
                pixelMatrix[i] = Arrays.copyOfRange(pixels, bufferedImage.getWidth()*i, bufferedImage.getWidth()*i+bufferedImage.getWidth());
            }
            for(int i=0; i<bufferedImage.getHeight(); i++){
                for(int j=0; j<bufferedImage.getWidth(); j++){
                    if (pixelMatrix[i][j] == -1)
                        pixelMatrix[i][j] = 0;
                    else
                        pixelMatrix[i][j] = 1;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pixelMatrix;
    }
}
