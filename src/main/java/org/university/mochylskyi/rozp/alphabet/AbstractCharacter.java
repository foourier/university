package org.university.mochylskyi.rozp.alphabet;

import java.util.Arrays;
import java.util.Objects;

public abstract class AbstractCharacter implements Character {
    protected int[][] pixelMatrix;
    protected int rowCount;
    protected int columnCount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractCharacter that = (AbstractCharacter) o;
        return rowCount == that.rowCount &&
                columnCount == that.columnCount &&
                Arrays.equals(pixelMatrix, that.pixelMatrix);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(rowCount, columnCount);
        result = 31 * result + Arrays.hashCode(pixelMatrix);
        return result;
    }

    @Override
    public int[][] getMatrix() {
        return pixelMatrix;
    }

    @Override
    public int getCurrentPixel(int row, int column) {
        return pixelMatrix[row][column];
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (int [] a: pixelMatrix)
            for (int b: a)
                if(b == 1)
                    count++;
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int [] a: pixelMatrix) {
            for (int b : a)
                sb.append(b);
            sb.append('\n');
        }
        return sb.toString();
    }
}
