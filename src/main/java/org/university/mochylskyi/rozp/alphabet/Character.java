package org.university.mochylskyi.rozp.alphabet;

public interface Character {
    int[][] getMatrix();
    int getCurrentPixel(int row, int column);
    char getCharacter();
    int getRowCount();
    int getColumnCount();
    int getCount();
}
