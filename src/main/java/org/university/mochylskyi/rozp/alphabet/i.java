package org.university.mochylskyi.rozp.alphabet;

import org.university.mochylskyi.rozp.ImageReader;
import java.io.File;

public class i extends AbstractCharacter implements Character{

    public i(){
        pixelMatrix = ImageReader.read(new File("src\\main\\resources\\moch\\alph-16\\i.png"));
        rowCount = pixelMatrix.length;
        columnCount = pixelMatrix[0].length;
    }

    @Override
    public char getCharacter() {
        return 'i';
    }
}
