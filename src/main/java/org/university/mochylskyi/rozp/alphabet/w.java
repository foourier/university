package org.university.mochylskyi.rozp.alphabet;

import org.university.mochylskyi.rozp.ImageReader;
import java.io.File;

public class w extends AbstractCharacter implements Character{

    public w(){
        pixelMatrix = ImageReader.read(new File("src\\main\\resources\\moch\\alph-16\\w.png"));
        rowCount = pixelMatrix.length;
        columnCount = pixelMatrix[0].length;
    }

    @Override
    public char getCharacter() {
        return 'w';
    }
}
