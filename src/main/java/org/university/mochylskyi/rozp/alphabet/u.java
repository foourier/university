package org.university.mochylskyi.rozp.alphabet;

import org.university.mochylskyi.rozp.ImageReader;
import java.io.File;

public class u extends AbstractCharacter implements Character{

    public u(){
        pixelMatrix = ImageReader.read(new File("src\\main\\resources\\moch\\alph-16\\u.png"));
        rowCount = pixelMatrix.length;
        columnCount = pixelMatrix[0].length;
    }

    @Override
    public char getCharacter() {
        return 'u';
    }
}
