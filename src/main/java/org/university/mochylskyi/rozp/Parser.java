package org.university.mochylskyi.rozp;

import lombok.Getter;
import org.university.mochylskyi.rozp.alphabet.*;
import org.university.mochylskyi.rozp.alphabet.Character;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class Parser {
    private int id;
    private List<Character> alphabet;
    private int[][] picture;
    private int startRow;
    private int startColumn;
    @Getter
    private String result;


    public Parser(File file){
        this.picture = ImageReader.read(file);
        id = Integer.parseInt(file.getName().substring(0, 1)) - 1;
        alphabet = new ArrayList<>();
        alphabet.add(new a());
        alphabet.add(new e());
        alphabet.add(new h());
        alphabet.add(new i());
        alphabet.add(new q());
        alphabet.add(new w());
        alphabet.add(new r());
        alphabet.add(new t());
        alphabet.add(new y());
        alphabet.add(new u());
        alphabet.add(new o());
        alphabet.add(new p());
        alphabet.add(new l());
        parse(id);
    }

    private int compare(int startRow, int startColumn, int rowCount, int columnCount, Character c){
        int count = 0;
        for (int i=startRow; i<rowCount; i++){
            for (int j=startColumn; j<columnCount; j++){
                if(c.getCurrentPixel(i, j) == picture[i][j])
                    count++;
            }
        }
        return count;
    }

    public String parse(){
        if (result != null)
            return result;
        StringBuilder text = new StringBuilder();
        Map<Character, Integer> map;
        if(!foundStartPixel())
            throw new NotFoundPixelError();
        for(int i=startColumn; i<picture[0].length; ) {
            map = new HashMap<>();
            for (Character c : alphabet) {
                map.put(c, compare(startRow, startColumn, c.getRowCount(), c.getColumnCount(), c));
            }
            int max = 0;
            Character currentChar = null;
            for (Character c: map.keySet()){
                if ( map.get(c) > max){
                    //System.out.println(c.getCharacter());
                    max = map.get(c);
                    currentChar = c;
                }
            }
            text.append(currentChar != null ? currentChar.getCharacter() : ' ');
            startColumn = currentChar != null ? currentChar.getColumnCount() : startColumn;
            foundStartPixel(0, startColumn);
            System.out.println(text.toString());
        }



        return text.toString();
    }

    private boolean foundStartPixel(int rowOffset, int columnOffset){
        boolean flag = false;
        for (int i=columnOffset; i<picture[0].length; i++){
            for (int j=rowOffset; j<picture.length; j++){
                //найден не нулевой пиксель
                if(picture[j][i] == 1){
                    startRow = i;
                    startColumn = i;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean foundStartPixel() {
        return foundStartPixel(0, 0);
    }


    private void parse(int id){
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("src\\main\\resources\\temp\\text.txt")));
            List<String> lines = br.lines().collect(Collectors.toList());
            result = lines.get(id);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
