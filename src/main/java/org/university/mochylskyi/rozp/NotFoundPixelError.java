package org.university.mochylskyi.rozp;

public class NotFoundPixelError extends Error {
    public NotFoundPixelError() {
        super("Pixel not found!!!");
    }
}
