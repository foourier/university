package org.university.lubun.aproximation;

import org.university.presentation.ShowLine;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class TestNetwork {
    private List<Double> x;
    private List<Double> y;
    private int prediction;
    private int n;

    private double ymin, ymax;

    private List<Double> normal;
    private DataUtil data;
    private double h;


    private Perseptron p;

    public TestNetwork(DataUtil data, Perseptron p, int prediction, int n) {
        this.data = data;
        this.h = data.getH();
        this.x = new ArrayList<>();
        this.y = new ArrayList<>();
        this.normal = new ArrayList<>();
        this.p = p;
        this.ymin = data.getYmin();
        this.ymax = data.getYmax();
        this.prediction = prediction;
        this.n = n;
    }

    public void test() {
        x = data.getX().subList(0, n-prediction);
        y = new ArrayList<>(data.getY().subList(0, n-prediction));
        normal = data.getNormal().subList(0, n-prediction);

        for (int i = 0; i < prediction; i++) {
            normal.add(p.getOut(normal.subList(normal.size() - p.getWIDTH_OF_WINDOW(), normal.size())));
            x.add(x.get(x.size() - 1) + h);
            y.add(deNormalize(normal.get(x.size() - 1)));
        }
    }

    public void show() {
        ShowLine.show("SIN with prediction", x, y);
    }

    private double normalize(double value) {
        return (value - ymin) / (ymax - ymin) * 0.4 + 0.3;
    }

    private double deNormalize(double value) {
        return (((value - 0.3) / 0.4) * (ymax - ymin)) + ymin;
    }

    public void setYmin(double ymin) {
        this.ymin = ymin;
    }

    public void setYmax(double ymax) {
        this.ymax = ymax;
    }

    public List<Double> getX() {
        return x;
    }

    public List<Double> getY() {
        return y;
    }

    public List<Double> getNormal() {
        return normal;
    }
}
