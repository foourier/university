package org.university.lubun.aproximation;

import java.util.List;

public class Perseptron {
    private final double ALPHA = 0.7;
    private final double ETA = 0.7;
    private final double EPS = 0.001;
    private final int COUNT_OF_LAYER = 2;
    private final int WIDTH_OF_WINDOW = 3;
    private final int COUNT_OF_OUTPUT = 5;

    private double[][] weightForInputLayer;
    private double[] weightForOutputLayer;
    private List<Double> testData;

    public Perseptron(List<Double> testData) {
        this.testData = testData;
        this.weightForInputLayer = new double[COUNT_OF_OUTPUT][WIDTH_OF_WINDOW];
        this.weightForOutputLayer = new double[COUNT_OF_OUTPUT];
        init();
    }

    public double getOut(final List<Double> data) {
        double[] inputForOutputLayer = getInputForOutputLayer(data);
        return activation(summatorForOutputLayer(inputForOutputLayer));
    }

    private double[] getInputForOutputLayer(final List<Double> data) {
        double[] inputForOutputLayer = new double[COUNT_OF_OUTPUT];
        for (int i = 0; i < COUNT_OF_OUTPUT; i++) {
            inputForOutputLayer[i] = activation(summatorForInputLayer(data, i));
        }
        return inputForOutputLayer;
    }

    private double summatorForInputLayer(final List<Double> input, int number) {
        double sum = 0;
        for (int i = 0; i < WIDTH_OF_WINDOW; i++) {
            sum += weightForInputLayer[number][i] * input.get(i);
        }
        return sum;
    }

    private double summatorForOutputLayer(double[] inputForOutputLayer) {
        double sum = 0;
        for (int i = 0; i < COUNT_OF_OUTPUT; i++) {
            sum += weightForOutputLayer[i] * inputForOutputLayer[i];
        }
        return sum;
    }

    private double activation(double s) {
        return 1.0 / (1.0 + Math.exp(-ALPHA * s));
    }

    public void teach() {
        boolean flag = false;
        int count = 0;
        for (int i = 0; i < testData.size() - WIDTH_OF_WINDOW - 1; i++) {
            List<Double> window = testData.subList(i, i + WIDTH_OF_WINDOW + 1);
            double out = getOut(window);
            while (!isLessThanEpsilon(out, window.get(window.size() - 1))) {
                double[] inputForOutputLayer = getInputForOutputLayer(window);
                double sigmaForOutputLayer = getSigmaForOutputLayer(out, window.get(window.size() - 1));
                double[] sigmaForInputLayer = getSigmaForInputLayer(sigmaForOutputLayer, inputForOutputLayer);

                recalcWeightForInputLayer(sigmaForInputLayer, window);
                recalcWeightForOutputLayer(sigmaForOutputLayer, inputForOutputLayer);

                out = getOut(window);
                flag = true;
                count++;
                if (count % 10000000 == 0) {
                    System.out.println(count / 10000000);
                }
            }
            if (flag) {
                i = -1;
                flag = false;
            }
        }
        System.out.println(count);
    }

    private void recalcWeightForOutputLayer(double sigma, double[] inputForOutputLayer) {
        for (int i = 0; i < COUNT_OF_OUTPUT; i++) {
            weightForOutputLayer[i] = calcNewWeight(weightForOutputLayer[i], sigma, inputForOutputLayer[i]);
        }
    }

    private void recalcWeightForInputLayer(double[] sigma, final List<Double> inputForInputLayer) {
        for (int j = 0; j < COUNT_OF_OUTPUT; j++) {
            for (int i = 0; i < WIDTH_OF_WINDOW; i++) {
                weightForInputLayer[j][i] = calcNewWeight(weightForInputLayer[j][i], sigma[j], inputForInputLayer.get(i));
            }
        }
    }

    private boolean isLessThanEpsilon(double out, double expected) {
        return (Math.pow((out - expected), 2) / 2.0) < EPS;
    }

    public double differents(double out, double expected) {
        return Math.pow((out - expected), 2) / 2.0;
    }

    private double getSigmaForOutputLayer(double out, double expected) {
        return (out - expected) * out * (1 - out);
    }

    private double[] getSigmaForInputLayer(double sigmaForOutLayer, double[] inputForOutputLayer) {
        double[] sigma = new double[COUNT_OF_OUTPUT];
        for (int i = 0; i < COUNT_OF_OUTPUT; i++) {
            sigma[i] = sigmaForOutLayer * weightForOutputLayer[i] * inputForOutputLayer[i] * (1 - inputForOutputLayer[i]);
        }
        return sigma;
    }

    private double calcNewWeight(double oldWeight, double sigma, double input) {
        return oldWeight - ETA * sigma * input;
    }

    private void init() {
        initWeight();
    }

    private void initWeight() {
        for (int j = 0; j < COUNT_OF_OUTPUT; j++) {
            for (int i = 0; i < WIDTH_OF_WINDOW; i++) {
                weightForInputLayer[j][i] = Math.random();
            }
        }
        for (int i = 0; i < COUNT_OF_OUTPUT; i++) {
            weightForOutputLayer[i] = Math.random();
        }
    }

    public int getWIDTH_OF_WINDOW() {
        return WIDTH_OF_WINDOW;
    }
}
