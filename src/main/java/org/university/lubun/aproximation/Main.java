package org.university.lubun.aproximation;

import org.university.presentation.ShowLine;

import java.util.List;
import java.util.Random;
import java.util.function.UnaryOperator;

public class Main {

    private static final int PREDICTION = 10;
    private static final int N = 50;

    public static void main(String[] args) {
        DataUtil testData = new DataUtil(0, 3 * Math.PI, N);

        UnaryOperator<Double> function = x -> {
            Random r = new Random();
            double nextR = r.nextGaussian();
            return Math.sin(x) + nextR / 10.0;

        };

        testData.tabulate(function);

        ShowLine.show("Sin with noise", testData.getX(), testData.getY());
        ShowLine.show("Sin with noise for teach", testData.getX().subList(0, N-PREDICTION), testData.getY().subList(0, N-PREDICTION));

        Perseptron p = new Perseptron(testData.getNormal().subList(0, N-PREDICTION));
        p.teach();

        TestNetwork testNetwork = new TestNetwork(testData, p, PREDICTION, N);
        testNetwork.test();
        testNetwork.show();
        compare(testData.getY().subList(N-PREDICTION, N), testNetwork.getY().subList(N-PREDICTION, N));
    }

    private static void compare(List<Double> exp, List<Double> act){
        for (int i=0; i<exp.size(); i++){
            System.out.println(String.format("exp = %.5f\tact = %.5f\teps = %.5f", exp.get(i), act.get(i), differents(exp.get(i), act.get(i))));
        }
    }

    public static double differents(double out, double expected) {
        return Math.pow((out - expected), 2) / 2.0;
    }
}
