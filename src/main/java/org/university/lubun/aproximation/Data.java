package org.university.lubun.aproximation;

public class Data {

    private double x;
    private double y;

    public Data(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Data(double x) {
        this.x = x;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
