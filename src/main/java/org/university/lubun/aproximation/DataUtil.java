package org.university.lubun.aproximation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataUtil {
    private List<Double> x;
    private List<Double> y;

    private double ymin, ymax;

    private List<Double> normal;

    private double a, b, h;
    private int n;

    public DataUtil(double a, double b, int n) {
        this.a = a;
        this.b = b;
        this.n = n;
        this.x = new ArrayList<>();
        this.y = new ArrayList<>();
        this.normal = new ArrayList<>();
    }

    public void tabulate(UnaryOperator<Double> function) {
        this.h = (b - a) / (double) n;
        x = Stream.iterate(a, n -> n + h).limit(n).collect(Collectors.toList());
        y = x.stream().map(function).collect(Collectors.toList());
        y.stream().mapToDouble(Double::doubleValue).max().ifPresent(this::setYmax);
        y.stream().mapToDouble(Double::doubleValue).min().ifPresent(this::setYmin);
        normal = y.stream().map(this::normalize).collect(Collectors.toList());
    }

    private double normalize(double value) {
        return (value - ymin) / (ymax - ymin) * 0.4 + 0.3;
    }

    public void setYmin(double ymin) {
        this.ymin = ymin;
    }

    public void setYmax(double ymax) {
        this.ymax = ymax;
    }

    public List<Double> getX() {
        return x;
    }

    public List<Double> getY() {
        return y;
    }

    public List<Double> getNormal() {
        return normal;
    }

    public void setX(List<Double> x) {
        this.x = x;
    }

    public void setY(List<Double> y) {
        this.y = y;
    }

    public double getYmin() {
        return ymin;
    }

    public double getYmax() {
        return ymax;
    }

    public void setNormal(List<Double> normal) {
        this.normal = normal;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
