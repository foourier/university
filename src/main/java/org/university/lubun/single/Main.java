package org.university.lubun.single;

import org.university.lubun.single.Data;
import org.university.lubun.single.Perseptron;

public class Main {

    public static void main(String[] args) {
        Perseptron p = new Perseptron();

        p.teach();
        System.out.println(p.getResult(new Data(0, 0)));
        System.out.println(p.getResult(new Data(1, 0)));
        System.out.println(p.getResult(new Data(0, 1)));
        System.out.println(p.getResult(new Data(1, 1)));
        p.show(0, 1);

    }
}
