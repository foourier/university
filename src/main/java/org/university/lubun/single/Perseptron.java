package org.university.lubun.single;

import java.util.ArrayList;

public class Perseptron extends AbstractPerseptron {


    private static final double ALPHA = 0.5;
    private static final double ETA = 0.1;
    private static final double EPS = 0.00001;

    public Perseptron() {
        testData = new ArrayList<>();
        weight = new double[3];
        init();
    }

    private void init() {
        initTestData();
        initWeight();
    }

    private void initTestData() {
        testData.add(new Data(0, 0, 0));
        testData.add(new Data(1, 0, 1));
        testData.add(new Data(0, 1, 1));
        testData.add(new Data(1, 1, 1));
    }

    private void initWeight() {
        weight[0] = Math.random();
        weight[1] = Math.random();
        weight[2] = Math.random();
    }


}
