package org.university.lubun.single;


import java.util.ArrayList;

public class Clasificator extends AbstractPerseptron {
    private static final double ALPHA = 1;
    private static final double ETA = 0.7;
    private static final double EPS = 0.1;

    public Clasificator(Data a, Data b) {
        testData = new ArrayList<>();
        weight = new double[3];
        init(a,b);
    }

    private void init(Data a, Data b) {
        initTestData(a, b);
        initWeight();
    }

    private void initTestData(Data a, Data b) {
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));
        testData.add(new Data(a.getX1()+Math.random(), a.getX2()+Math.random(), 1));

        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
        testData.add(new Data(b.getX1()+Math.random(), b.getX2()+Math.random(), 0));
    }

    private void initWeight() {
        weight[0] = Math.random();
        weight[1] = Math.random();
        weight[2] = Math.random();
    }


}
