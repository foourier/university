package org.university.lubun.single;

public class Data {
    private double x1;
    private double x2;
    private double xn;
    private double y;

    public Data(double x1, double x2, double y) {
        this.x1 = x1;
        this.x2 = x2;
        this.xn = 1;
        this.y = y;
    }

    public Data(double x1, double x2) {
        this.x1 = x1;
        this.x2 = x2;
        this.xn = 1;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getXn() {
        return xn;
    }

    public void setXn(double xn) {
        this.xn = xn;
    }

}
