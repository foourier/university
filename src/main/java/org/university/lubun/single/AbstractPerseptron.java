package org.university.lubun.single;

import org.university.presentation.ShowLine;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractPerseptron {
    protected final double ALPHA = 0.5;
    protected final double ETA = 0.1;
    protected final double EPS = 0.00001;

    protected double[] weight;
    protected List<Data> testData;

    public double getResult(Data data) {
        return activation(summator(data));
    }

    protected double summator(Data input) {
        return weight[0] * input.getX1() + weight[1] * input.getX2() + weight[2];
    }

    protected double activation(double s) {
        return 1.0 / (1.0 + Math.exp(-ALPHA * s));
    }

    public void teach() {
        boolean flag = false;
        int count = 0;
        for (int i = 0; i < testData.size(); i++) {
            double out = activation(summator(testData.get(i)));
            while (!isLessThanEpsilon(out, testData.get(i).getY())) {
                recalcWeight(testData.get(i), out);
                out = activation(summator(testData.get(i)));
                flag = true;
                count++;
            }
            if (flag) {
                i = -1;
                flag = false;
            }
        }
        System.out.println(count);
    }

    protected void recalcWeight(Data testData, double out) {
        weight[0] = calcNewWeght(weight[0], calcCoeficient(out, testData.getY(), testData.getX1()));
        weight[1] = calcNewWeght(weight[1], calcCoeficient(out, testData.getY(), testData.getX2()));
        weight[2] = calcNewWeght(weight[2], calcCoeficient(out, testData.getY(), testData.getXn()));
    }

    protected boolean isLessThanEpsilon(double out, double expected) {
        return (Math.pow((out - expected), 2) / 2.0) < EPS;
    }

    protected double calcCoeficient(double out, double t, double x) {
        return (out - t) * out * (1 - out) * x;
    }

    protected double calcNewWeght(double oldWeight, double coeficient) {
        return oldWeight - ETA * coeficient;
    }

    public void show(double x1, double x2){
        double y1 = (-x1 * weight[0] - weight[2]) / weight[1];
        double y2 = (-x2 * weight[0] - weight[2]) / weight[1];
        ShowLine.show("Line", Arrays.asList(x1, x2), Arrays.asList(y1, y2));
    }
}
