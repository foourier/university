package org.university.rabyk.filter;

import org.university.presentation.ShowLine;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

class Filter {
    private static final double F = 10.0;
    private static final double F_DISC = 48.0;
    private static final double WIDTH = 0.5;
    private static final double F_DIS = 8.0;
    private static final int N = 53;


    private static final double DELTA_F = WIDTH / F_DIS;

    private static final double FC = F + DELTA_F / 2.0;
    private static final double F_NORMAL = FC / F_DISC;
    private static final double HD0 = 1 - 2 * F_NORMAL;
    public static final int SIZE = N / 2;

    private List<Double> frequencyResponse;
    private List<Double> phaseResponse;
    private List<Double> x;


    private List<Double> impulseCoeficient;

    public List<Double> getImpulseCoeficient() {
        return impulseCoeficient;
    }


    public Filter() {
        impulseCoeficient = new ArrayList<>();
        frequencyResponse = new ArrayList<>();
        phaseResponse = new ArrayList<>();
        x = new ArrayList<>();
    }

    private double calcHd(int n) {
        double result = 1 - 2 * F_NORMAL;
        if (n != 0)
            result = (-2 * F_NORMAL * Math.sin(n * omega(n)))
                    / (n * omega(n));
        return result;
    }

    private double omega(int n) {
        return 0.5 + 0.5 * Math.cos((2.0 * Math.PI * n) / (double) N);
    }

    public void calculateImpulseCoeficient() {
        for (int i = 0; i < N / 2; i++) {
            impulseCoeficient.add(calcHd(i) * omega(i));
        }
        for (int i = 0; i < N / 2; i++) {
            impulseCoeficient.add(impulseCoeficient.get(i));
        }
    }

    public void calcFrequencyAndPhaseResponse() {
        double sam = 6 * 1000 * 3.1;
        for (int j = 0; j < sam / 2; ++j) {
            double wi = 2 * Math.PI * j / sam;
            double HRe = 0d;
            double HIm = 0d;
            for (int i = 1; i < N / 2; ++i) {
                HRe += impulseCoeficient.get(i) * Math.cos(wi * -i);
                HIm -= impulseCoeficient.get(i) * Math.sin(wi * -i);
            }
            HRe += impulseCoeficient.get(0) * Math.cos(wi * 0);
            HIm -= impulseCoeficient.get(0) * Math.sin(wi * 0);
            for (int i = 1; i < N / 2; ++i) {
                HRe += impulseCoeficient.get(i) * Math.cos(wi * i);
                HIm -= impulseCoeficient.get(i) * Math.sin(wi * i);
            }
            frequencyResponse.add(Math.log10(Math.sqrt(HRe * HRe + HIm * HIm)) * 20);
            phaseResponse.add(HIm == 0 ? 0 : Math.atan(HIm / HRe));
            x.add((double) j);
        }
    }

    public void showFrequencyResponse() {
        ShowLine.show("Frequency response", x, frequencyResponse);
    }

    public void showPhaseResponse() {
        ShowLine.show("Phase response", x, phaseResponse);
    }
}