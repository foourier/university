package org.university.rabyk.filter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Double> coeficient = readCoef();
        Signal signal = new Signal(coeficient);
        signal.generateInputSignal();
        signal.generateOutputSignal();
        signal.showInputAndOutputSignal();
        signal.showNoise();
    }

    private static List<Double> readCoef(){
        List<Double> coeficient = new ArrayList<>();
        StringBuilder igText = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("D:\\git\\university\\src\\main\\resources\\rabyk\\coef.txt"));
            reader.lines().forEach(igText::append);
            coeficient = Arrays.stream(igText.toString().split(",")).map(Double::valueOf).collect(Collectors.toList());
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        return coeficient;

    }

}
