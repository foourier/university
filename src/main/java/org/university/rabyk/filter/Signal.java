package org.university.rabyk.filter;

import org.university.presentation.ShowLine;

import java.util.ArrayList;
import java.util.List;

public class Signal {
    private static final double[] FREQUENCY = {0.25, 0.5, 1.0, 1.5, 2.5, 4.0};
    private static final double[] AMPLITUDE = {0.5, 1.0, 3.0, 4.0, 2.5, 1.5};
    private static final int SIZE = 1024;
    private List<Double> inputSignal;
    private List<Double> outputSignal;
    private List<Double> inpulsCoeficient;
    private List<Double> time;

    public Signal(List<Double> inpulsCoeficient) {
        inputSignal = new ArrayList<>();
        outputSignal = new ArrayList<>();
        time = new ArrayList<>();
        this.inpulsCoeficient = inpulsCoeficient;
    }

    public void generateInputSignal() {
        double t = 0;
        double h = 1d / (240000d);
        double sum = 0;
        for (int i = 0; i < SIZE; i++) {
            sum = 0;
            for (int j = 0; j < FREQUENCY.length; j++) {
                sum += AMPLITUDE[j] * Math.sin(2 * Math.PI * FREQUENCY[j] * 1e3 * t);
            }
            inputSignal.add(sum);
            time.add(t);
            t += h;
        }
    }

    public void generateOutputSignal() {
        double sum = 0;
        for (int i = 0; i < SIZE; i++) {
            sum = 0;
            for (int j = 0; j < Filter.SIZE; j++) {
                sum += j <= i ? inpulsCoeficient.get(j) * inputSignal.get(i - j) : 0;
            }
            outputSignal.add(sum);
        }
    }

    public void showInputAndOutputSignal() {
        List<List<Double>> signals = new ArrayList<>();
        signals.add(inputSignal);
        signals.add(outputSignal);
        ShowLine.show("Input signal", time, signals, true);
    }

    public void showNoise() {
        List<Double> noise = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            noise.add(inputSignal.get(i) - outputSignal.get(i));
        }
        ShowLine.show("Noise", time, noise);
    }
}
