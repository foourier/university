//коефіцієнти
Fp=10.0; //  гранична смуга пропускання
fs=48; // ширина смуги переходу
dF=0.5; // затухання в смузі подавлення
n=53; // частота дискретизації
fn=(Fp-dF/2)/fs;
[wft, wfm, fr]=wfir('hp',n,[fn 0],'hm',[0 0]);
//АЧХ
subplot(3,2,1)
plot2d(fr, 20*log10(wfm)); 
xtitle('Highpass filter with Hamming window','norm. freq', 'Amplitude (Db)');
//ФЧХ
n1 = length(fr); 
for k=1:1:n1      
	sum_chus = 0;      
	sum_znam = 0;      
	for j=1:1:n         
		w=2*%pi*fr(k)*j;         
		sum_chus = sum_chus + wft(j)*sin(w);         
		sum_znam = sum_znam + wft(j)*cos(w);      
	end      
	rez(k) = -atan(sum_chus/sum_znam); 
end 
subplot(3,2,2)
plot2d(fr, 180.0*rez/(%pi)); 
xtitle('Highpass filter with Hamming window','norm. freq (kHz)', 'Phaza (degre)')
// Імпульсна характеристика фільтра 
for k=1:1:n 
	tr(k) = k;    
end 
subplot(3,2,3) 
plot2d(tr, wft); 
xlabel ('n') 
ylabel ('Impulse response') 
title ('Low pass filter with Hamming window')
