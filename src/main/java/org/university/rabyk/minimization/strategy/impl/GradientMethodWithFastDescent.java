package org.university.rabyk.minimization.strategy.impl;

import org.university.rabyk.minimization.strategy.Function;
import org.university.rabyk.minimization.strategy.MinimizationStrategyForOneArg;
import org.university.rabyk.minimization.strategy.MinimizationStrategyForTowArg;

import static java.lang.Math.pow;

public class GradientMethodWithFastDescent implements MinimizationStrategyForTowArg {
    private double x1;
    private double x2;
    private double y;
    private int count;
    private MinimizationStrategyForOneArg min;

    public GradientMethodWithFastDescent() {
        this.min = new GoldRatioStrategy();
    }

    @Override
    public void findMin(double[] startX, double defaultStep) {
        double[] gradient = new double[2];
        count = 1;
        gradient[0] = (dfdx1(startX[0], startX[1]));
        gradient[1] = (dfdx2(startX[0], startX[1]));
        while (normOfVector(gradient) > EPS) {

            defaultStep = minimum(startX, gradient);

            startX[0] -= defaultStep * gradient[0];
            startX[1] -= defaultStep * gradient[1];
            gradient[0] = (dfdx1(startX[0], startX[1]));
            gradient[1] = (dfdx2(startX[0], startX[1]));
            count++;
        }
        x1 = startX[0];
        x2 = startX[1];
        y = function(x1, x2);
    }

    private double minimum(final double[] startX, final double[] gradient) {
        Function f = alpha -> {
            double x1 = startX[0] - alpha * gradient[0];
            double x2 = startX[1] - alpha * gradient[1];
            return pow((x2 - pow(x1, 2)), 2) + pow((1 - x1), 2);
        };

        min.findMin(f, 0, 3);

        return min.getX();
    }

    @Override
    public double getX1() {
        return x1;
    }

    @Override
    public double getX2() {
        return x2;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public int getCount() {
        return count;
    }
}
