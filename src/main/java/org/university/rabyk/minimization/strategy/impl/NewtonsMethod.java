package org.university.rabyk.minimization.strategy.impl;

import org.university.rabyk.minimization.strategy.MinimizationStrategyForTowArg;

public class NewtonsMethod implements MinimizationStrategyForTowArg {
    private double x1;
    private double x2;
    private double y;
    private int count;


    @Override
    public void findMin(double[] startX, double defaultStep) {
        double[] gradient = new double[2];
        count = 1;
        gradient[0] = (dfdx1(startX[0], startX[1]));
        gradient[1] = (dfdx2(startX[0], startX[1]));
        while (normOfVector(gradient) > EPS) {
            calcNewX(gradient, startX);
            gradient[0] = (dfdx1(startX[0], startX[1]));
            gradient[1] = (dfdx2(startX[0], startX[1]));
            count++;
        }
        x1 = startX[0];
        x2 = startX[1];
        y = function(x1, x2);
    }

    private void calcNewX(double[] gradient, double[] startX) {
        double[][] hesse = getHesseMatrix(startX);
        double[][] revertHesse = getRevertMatrix(hesse);
        double revertDet = 1.0 / getDet(hesse);
        double[] alpha = multiplyMatrix(revertHesse, gradient);
        for (int i = 0; i < startX.length; i++)
            startX[i] -= revertDet * alpha[i];
    }

    private double[] multiplyMatrix(double[][] revertHesse, double[] startX) {
        double[] result = new double[startX.length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result.length; j++)
                result[i] += revertHesse[i][j] * startX[j];
        }
        return result;
    }

    private double[][] getHesseMatrix(double[] startX) {
        double[][] hesseMatrix = new double[2][2];

        hesseMatrix[0][0] = d2fdx1(startX[0], startX[1]);
        hesseMatrix[0][1] = d2fdx1x2(startX[0], startX[1]);
        hesseMatrix[1][0] = d2fdx2x1(startX[0], startX[1]);
        hesseMatrix[1][1] = d2fdx2(startX[0], startX[1]);

        return hesseMatrix;
    }

    private double getDet(double[][] matrix) {
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    }

    private double[][] getRevertMatrix(double[][] matrix) {
        double[][] revertMatrix = new double[2][2];

        revertMatrix[0][0] = matrix[1][1];
        revertMatrix[0][1] = -matrix[0][1];
        revertMatrix[1][0] = -matrix[1][0];
        revertMatrix[1][1] = matrix[0][0];

        return revertMatrix;
    }

    @Override
    public double getX1() {
        return x1;
    }

    @Override
    public double getX2() {
        return x2;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public int getCount() {
        return count;
    }
}
