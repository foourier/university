package org.university.rabyk.minimization.strategy;

import static java.lang.Math.pow;

public interface MinimizationStrategyForOneArg {
    double EPSILON = 1e-5;

//    default double function(double x) {
//        return pow(x, 3) + 6 * pow(x, 2) + 9 * x;
//    }

    void findMin(Function f, double a, double b);

    int getCount();

    double getX();

    double getY();
}
