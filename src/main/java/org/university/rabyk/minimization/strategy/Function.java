package org.university.rabyk.minimization.strategy;

@FunctionalInterface
public interface Function {
    double calc(double x);
}
