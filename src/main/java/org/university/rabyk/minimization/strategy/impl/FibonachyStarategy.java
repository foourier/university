package org.university.rabyk.minimization.strategy.impl;

import lombok.Getter;
import org.university.rabyk.minimization.strategy.Function;
import org.university.rabyk.minimization.strategy.MinimizationStrategyForOneArg;

public class FibonachyStarategy implements MinimizationStrategyForOneArg {
    private static final int N = 25;

    private int count;

    private double x;

    private double y;

    private int Fn;

    public FibonachyStarategy(int fn) {
        Fn = fn;
    }

    @Override
    public void findMin(Function f, double a, double b) {
        double x1, x2, y1, y2;
        int f1 = 1, f0 = 1, ff = 0;
        this.count = 0;

        while (ff < Fn) {
            ff = f1 + f0;
            if (ff < Fn) {
                f0 = f1;
                f1 = ff;
            }
        }

        x1 = a + f0 * (b - a) / ff;
        x2 = a + f1 * (b - a) / ff;

        y1 = f.calc(x1);
        y2 = f.calc(x2);

        do {
            if (y1 <= y2) {
                b = x2;
                x2 = x1;
                y2 = y1;
                x1 = a + b - x2;
                y1 = f.calc(x1);
            } else {
                a = x1;
                x1 = x2;
                y1 = y2;
                x2 = a + b - x1;
                y2 = f.calc(x2);
            }
            this.count++;
        }
        while (b - a > 2 * EPSILON);

        if (y1 <= y2)
            b = x2;
        else
            a = x1;


        x = (a + b) / 2;
        y = f.calc(x);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }
}
