package org.university.rabyk.minimization.strategy.impl;

import org.university.rabyk.minimization.strategy.MinimizationStrategyForTowArg;

public class GradientMethodWithDefaultStep implements MinimizationStrategyForTowArg {
    private double x1;
    private double x2;
    private double y;
    private int count;

    @Override
    public void findMin(double[] startX, double defaultStep) {
        double[] gradient = new double[2];
        count = 1;
        gradient[0] = (dfdx1(startX[0], startX[1]));
        gradient[1] = (dfdx2(startX[0], startX[1]));
        while ( normOfVector(gradient) > EPS) {
            startX[0] -= defaultStep * gradient[0];
            startX[1] -= defaultStep * gradient[1];
            gradient[0] = (dfdx1(startX[0], startX[1]));
            gradient[1] = (dfdx2(startX[0], startX[1]));
            count++;
        }
        x1 = startX[0];
        x2 = startX[1];
        y = function(x1, x2);
    }



    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getY() {
        return y;
    }

    public int getCount() {
        return count;
    }
}
