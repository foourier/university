package org.university.rabyk.minimization.strategy;

import java.util.Arrays;

import static java.lang.Math.pow;

public interface MinimizationStrategyForTowArg {
    double EPS = 0.00001;

    default double function(double x1, double x2) {
        return pow((x2 - pow(x1, 2)), 2) + pow((1 - x1), 2);
    }

    default double function(double[] x) {
        return function(x[0], x[1]);
    }

    default double dfdx1(double x1, double x2) {
        return 4 * pow(x1, 3) - 4 * x1 * x2 + 2 * x1 - 2;
    }

    default double dfdx2(double x1, double x2) {
        return 2 * x2 - 2 * pow(x1, 2);
    }

    default double d2fdx1(double x1, double x2) {
        return 12 * pow(x1, 2) - 4 * x2 + 2;
    }

    default double d2fdx2(double x1, double x2) {
        return 2;
    }

    default double d2fdx1x2(double x1, double x2) {
        return -4 * x1;
    }

    default double d2fdx2x1(double x1, double x2) {
        return -4 * x1;
    }

    default double normOfVector(double[] vector) {
        return Math.sqrt(Arrays.stream(vector).map(x -> x * x).sum());
    }

    void findMin(double[] startX, double defaultStep);

    double getX1();

    double getX2();

    double getY();

    int getCount();
}
