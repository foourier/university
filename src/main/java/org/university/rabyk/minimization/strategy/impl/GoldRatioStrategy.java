package org.university.rabyk.minimization.strategy.impl;

import lombok.Getter;
import org.university.rabyk.minimization.strategy.Function;
import org.university.rabyk.minimization.strategy.MinimizationStrategyForOneArg;

public class GoldRatioStrategy implements MinimizationStrategyForOneArg {
    private static final double K = 0.618;
    private int count;
    private double x;
    private double y;


    @Override
    public void findMin(Function f, double a, double b) {
        double x1, x2;
        this.count = 0;
        while (Math.abs(b - a) > EPSILON) {
            x1 = b - K * (b - a);
            x2 = a + K * (b - a);

            if (f.calc(x1) > f.calc(x2)) {
                a = x1;
            } else {
                b = x2;
            }
            this.count++;
        }

        x = (a + b) / 2;
        y = f.calc(x);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }
}
