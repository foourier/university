package org.university.rabyk.minimization;

import org.university.rabyk.minimization.strategy.Function;
import org.university.rabyk.minimization.strategy.MinimizationStrategyForOneArg;
import org.university.rabyk.minimization.strategy.MinimizationStrategyForTowArg;
import org.university.rabyk.minimization.strategy.impl.*;

public class Main {
    private static Function f = x -> Math.pow(x, 3) + 6 * Math.pow(x, 2) + 9 * x;

    public static void main(String[] args) {
        MinimizationStrategyForOneArg gold = new GoldRatioStrategy();
        MinimizationStrategyForOneArg fibonachi = new FibonachyStarategy(5000);

        gold.findMin(f, -3, 0);
        fibonachi.findMin(f, -3, 0);
        System.out.println(String.format("GoldRatioStrategy  :\tx = %-4s; f(x) = %-4s; count = %d", gold.getX(), gold.getY(), gold.getCount()));
        System.out.println(String.format("FibonachyStarategy :\tx = %-4s; f(x) = %-4s; count = %d", fibonachi.getX(), fibonachi.getY(), fibonachi.getCount()));

        MinimizationStrategyForTowArg gradient = new GradientMethodWithDefaultStep();
        MinimizationStrategyForTowArg gradientWithStep = new GradientMethodWithDividedStep();
        MinimizationStrategyForTowArg fastGradient = new GradientMethodWithFastDescent();
        MinimizationStrategyForTowArg newtons = new NewtonsMethod();
        gradient.findMin(new double[]{-1.2, 1.0}, 0.1);
        gradientWithStep.findMin(new double[]{-1.2, 1.0}, 0.1);
        fastGradient.findMin(new double[]{-1.2, 1.0}, 0.1);
        newtons.findMin(new double[]{-1.2, 1.0}, 0.1);

        System.out.println(String.format("GradientMethodWithDefaultStep :\tx1 = %-4s; x2 = %-4s; f(x1, x2) = %-4s; count = %d", gradient.getX1(), gradient.getX2(), gradient.getY(), gradient.getCount()));
        System.out.println(String.format("GradientMethodWithDividedStep :\tx1 = %-4s; x2 = %-4s; f(x1, x2) = %-4s; count = %d", gradientWithStep.getX1(), gradientWithStep.getX2(), gradientWithStep.getY(), gradientWithStep.getCount()));
        System.out.println(String.format("GradientMethodWithFastDescent :\tx1 = %-4s; x2 = %-4s; f(x1, x2) = %-4s; count = %d", fastGradient.getX1(), fastGradient.getX2(), fastGradient.getY(), fastGradient.getCount()));
        System.out.println(String.format("NewtonsMethod :\tx1 = %-4s; x2 = %-4s; f(x1, x2) = %-4s; count = %d", newtons.getX1(), newtons.getX2(), newtons.getY(), newtons.getCount()));
    }
}
