package org.university.rabyk.lms;

import org.university.presentation.ShowLine;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class SignalUtils {

    public static List<Double> generateNoise(int L) {
        List<Double> noise = new ArrayList<>();
        List<Double> x = new ArrayList<>();
        Random r = new Random();

        for (int i=0; i<L; i++){
            noise.add(r.nextGaussian());
            x.add((double)i);
        }
        OptionalDouble d = noise.stream().mapToDouble(Math::abs).max();
        if (d.isPresent())
            noise = noise.stream().map(value -> value/(d.getAsDouble()*10)).collect(Collectors.toList());

        ShowLine.show("noise", x, noise);

        return noise;
    }

    public static List<Double> generateSignal(int L, UnaryOperator<Double> func){
        List<Double> signal = new ArrayList<>();
        List<Double> x = new ArrayList<>();

        for (int i=0; i<L; i++){
            signal.add(func.apply((double)i));
            x.add((double)i);
        }

        ShowLine.show("signal", x, signal);

        return signal;
    }

    public static List<Double> generateSignalWithNoise(List<Double> signal, List<Double> noise){
        List<Double> signalWithNoise = new ArrayList<>();
        List<Double> x = new ArrayList<>();

        for(int i=0; i<signal.size(); i++){
            signalWithNoise.add(signal.get(i)+noise.get(i));
            x.add((double)i);
        }

        ShowLine.show("signal with noise", x, signalWithNoise);

        return signalWithNoise;

    }

}
