package org.university.rabyk.lms;

import org.university.presentation.ShowLine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Filter {
    private static final int N = 43;
    private static final int L = 1100;
    private static final double MU = 0.001;

    private List<Double> x;
    private List<Double> d;
    private List<Double> y;
    private List<Double> h;
    private List<Double> e;

    public Filter(List<Double> x, List<Double> d) {
        this.x = x;
        this.d = d;
        this.y = new ArrayList<>();
        this.e = new ArrayList<>();
        this.h = new ArrayList<>();
        h.add(0d);
    }

    public void calc() {
        for (int k = 0; k < L; k++) {
            y.add(calcY(k));
            double eps = d.get(k) - y.get(k);
            e.add(eps);
            h.add(h.get(k) + MU * eps * x.get(k));
        }
    }

    private double calcY(int k) {
        double sum = 0;
        for (int i = 0; i < N; i++) {
            sum += k - i >= 0 ? h.get(i) + x.get(k - i) : 0;
        }
        return sum;
    }

    private double calcY(int k, List<Double> s) {
        double sum = 0;
        for (int i = 0; i < N; i++) {
            sum += k - i >= 0 ? h.get(i) + s.get(k - i) : 0;
        }
        return sum;
    }

    public void filt(List<Double> signal, List<Double> noise){
        y = new ArrayList<>();
        e = new ArrayList<>();
        for (int k = 0; k < L; k++) {
            y.add(calcY(k, signal));
            double eps = d.get(k) - y.get(k);
            e.add(eps);
        }
    }

    public void show(){
        List<Double> X = Stream.iterate(0, i -> i+1).limit(L).map(v -> (double)v).collect(Collectors.toList());

        ShowLine.show("signal after fir", X, y);
        ShowLine.show("eps after fir", X, e);
    }

}
