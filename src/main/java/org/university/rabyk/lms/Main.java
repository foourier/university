package org.university.rabyk.lms;

import java.util.List;

public class Main {

    private static final int L = 1100;
    private static final int N = 43;

    public static void main(String[] args) {
        System.out.println(L);
        List<Double> whiteNoise = SignalUtils.generateNoise(L);
        List<Double> signal = SignalUtils.generateSignal(L, x -> 0.27 * Math.cos(2 * 3.14 * 40 * x * 1.0/(double)L));
        List<Double> signalWithNoise = SignalUtils.generateSignalWithNoise(signal, whiteNoise);
        Filter f = new Filter(signalWithNoise, signal);
        f.calc();
        f.filt(
                signal,
                SignalUtils.generateSignalWithNoise(signal, SignalUtils.generateNoise(L))
        );
        f.show();
    }

}
