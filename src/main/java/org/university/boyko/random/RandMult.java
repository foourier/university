package org.university.boyko.random;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RandMult implements Random {
    private List<Integer> randArr;

    private int a;
    private final int m = Integer.MAX_VALUE;

    public RandMult(int a){
        randArr = new ArrayList<>();
        this.a = a;
        randArr.add(268864);
    }

    public int getNextInt(){
        int nextInt;
        nextInt = Math.abs( (a * randArr.get(randArr.size()-1))%m );

        randArr.add( nextInt );
        return nextInt;
    }


    @Override
    public List<Integer> getArr() {
        return randArr;
    }

}
