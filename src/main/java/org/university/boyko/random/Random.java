package org.university.boyko.random;

import org.university.presentation.ShowGistagram;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface Random {
    int getNextInt();
    List<Integer> getArr();

    default void createGistagram() {
        List<Double> doubleArr = this.createDoubleArr();
        int []count = new int[11];
        for (double d: doubleArr){
            if(d >= 0 && d < 0.1 ){
                count[0]++;
            }else if(d >= 0 && d < 0.1 ){
                System.out.println("check");
                count[1]++;
            }else if(d >= 0.1 && d < 0.2 ){
                count[2]++;
            }else if(d >= 0.2 && d < 0.3 ){
                count[3]++;
            }else if(d >= 0.3 && d < 0.4 ){
                count[4]++;
            }else if(d >= 0.4 && d < 0.5 ){
                count[5]++;
            }else if(d >= 0.5 && d < 0.6 ){
                count[6]++;
            }else if(d >= 0.6 && d < 0.7 ){
                count[7]++;
            }else if(d >= 0.7 && d < 0.8 ){
                count[8]++;
            }else if(d >= 0.8 && d < 0.9 ){
                count[9]++;
            }else if(d >= 0.9 && d <= 1 ){
                count[10]++;
            }
        }

        List<Double> counts = Arrays.stream(count).mapToDouble(x -> x).boxed().collect(Collectors.toList());
        int[] x = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Double> X = Arrays.stream(x).mapToDouble(e -> e).boxed().collect(Collectors.toList());
        ShowGistagram.show(this.getClass().getName(), counts, X);

        double xsi = 0;
        double kolmogorov = -Double.MAX_VALUE;
        for (double d: doubleArr){
            xsi += Math.pow( (d - doubleArr.size()*0.1) , 2) / (doubleArr.size()*0.1);
            if( Math.abs(d/doubleArr.size() - 0.1) > kolmogorov )
                kolmogorov = Math.abs(d/doubleArr.size() - 0.1);
        }
        kolmogorov *= Math.sqrt(doubleArr.size());
        System.out.println(this.getClass().getName());
        System.out.println("xsi = " + xsi + "\nkolmogorov = " + kolmogorov);

    }


    default List<Double> createDoubleArr(){
        int max = Collections.max(getArr());
        List<Double> result = new ArrayList<>();
        for (int i: getArr()){
            result.add((double)i / (double)max);
        }
        return result;
    }

    default List<Integer> getSortedArr(){
        List<Integer> result = new ArrayList<>();
        Collections.copy(getArr(), result);
        Collections.sort(result);
        return result;
    }

    default void write(String name){
        File file = new File("src\\main\\resources\\boyko\\random\\"+name);
        try (Writer writer = new FileWriter(file)) {
            //file.createNewFile();
            for (double i: createDoubleArr()){
                writer.write(String.valueOf(i));
                writer.write('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
