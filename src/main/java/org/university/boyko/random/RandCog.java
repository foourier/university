package org.university.boyko.random;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandCog implements Random {
    private List<Integer> randArr;

    private int a;
    private final int m = Integer.MAX_VALUE;
    private int c;

    public RandCog(int a, int c){
        randArr = new ArrayList<>();
        this.a = a;
        this.c = c;
        randArr.add(268864);
    }

    @Override
    public int getNextInt() {
        int nextInt;
        nextInt = Math.abs( (a * randArr.get(randArr.size()-1) + c)%m );

        randArr.add( nextInt );
        return nextInt;
    }

    @Override
    public List<Integer> getArr() {
        return randArr;
    }
}
