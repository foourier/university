package org.university.boyko.random;


public class Main {
    public static void main(String[] args) {
        Random mult = new RandMult(24621);
        Random cog = new RandCog(5332, 11);
        Random adapCog = new RandAdapCog();
        for (int i=0; i<100; i++) {
            mult.getNextInt();
            cog.getNextInt();
            adapCog.getNextInt();
        }

        System.out.println("--------------------");

        adapCog.write("RandAdapCog.txt");
        mult.write("RandMult.txt");
        cog.write("RandCog.txt");

        adapCog.createGistagram();
        mult.createGistagram();
        cog.createGistagram();
    }

}
