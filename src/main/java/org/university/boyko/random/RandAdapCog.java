package org.university.boyko.random;

import org.university.presentation.ShowGistagram;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RandAdapCog implements Random {
    private List<Integer> randArr;

    private final int m = Integer.MAX_VALUE;

    public RandAdapCog(){
        randArr = new ArrayList<>();
        randArr.add(1996886);
        randArr.add(3540932);
        randArr.add(1544132);
        randArr.add(6425234);
        randArr.add(9345672);
        randArr.add(3256437);
        randArr.add(9803423);
        randArr.add(3575423);
        randArr.add(2364657);
        randArr.add(7356765);
        randArr.add(8573023);
        randArr.add(6495421);
        randArr.add(3213254);
        randArr.add(7531355);
        randArr.add(8645125);
        randArr.add(6423145);
    }

    @Override
    public int getNextInt() {
        int nextInt;

        nextInt = Math.abs( (randArr.get(randArr.size()-1) - randArr.get(randArr.size()-16)))%m ;

        randArr.add( nextInt );
        return nextInt;
    }


    @Override
    public List<Integer> getArr() {
        return randArr;
    }

}
