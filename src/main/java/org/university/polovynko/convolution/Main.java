package org.university.polovynko.convolution;

import org.university.presentation.ShowGistagram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static int[]xArray;
    private static int[]yArray;

    public static void main(String[] args) {

        xArray = new int[]{0, 8, 6};
        yArray = new int[]{4, 2, 2};
        showResult("first");
        convolution().forEach(x -> System.out.print(x + "   "));
        System.out.println("\n--------------------------------------------------------------------------");
        xArray = new int[]{3, 2, 4, 0, 1, 2, 8, 7};
        yArray = new int[]{1, 8, 7, 6, 4};
        showResult("second");
        convolution().forEach(x -> System.out.print(x + "   "));
        System.out.println("\n--------------------------------------------------------------------------");
        xArray = new int[]{3, 4, 6, 0, 9, 2, 1, 6, 4, 0};
        yArray = new int[]{0, 1, 2, 3, 6, 4};
        showResult("third");
        convolution().forEach(x -> System.out.print(x + "   "));

    }

    private static void showResult(String ident){
        ShowGistagram.show(ident + " x signal",
                Arrays.stream(xArray).mapToDouble(x -> x).boxed().collect(Collectors.toList()),
                Stream
                        .iterate(1, a -> a+1)
                        .limit(xArray.length)
                        .mapToDouble(x->x)
                        .boxed()
                        .collect(Collectors.toList()));

        ShowGistagram.show(ident + " y signal",
                Arrays.stream(yArray).mapToDouble(x -> x).boxed().collect(Collectors.toList()),
                Stream
                        .iterate(1, a -> a+1)
                        .limit(yArray.length)
                        .mapToDouble(x->x)
                        .boxed()
                        .collect(Collectors.toList()));

        ShowGistagram.show(ident + " result signal",
                convolution().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                Stream
                        .iterate(1, a -> a+1)
                        .limit(convolution().size())
                        .mapToDouble(x->x)
                        .boxed()
                        .collect(Collectors.toList()));
    }

    private static List<Integer> convolution(){
        List<Integer> res = new ArrayList<>();
        int sum = 0;
        int m = 0;
        int offset = 1;
        while(true){
            sum = 0;
            if(offset == xArray.length)
                break;
            if(m <= yArray.length-1)
                for (int j = 0; j <= m; j++)
                    sum += xArray[j] * yArray[yArray.length - 1 - m + j];
            else{
                for (int j=offset; j < xArray.length; j++) {
                    if(j-offset < yArray.length)
                        sum += xArray[j] * yArray[j - offset];
                    else
                        break;
                }
                offset++;
            }
            res.add(sum);
            m++;
        }

        return res;

    }

}


