package org.university.polovynko.fourier;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        dfsDemonstration();
        System.out.println("------------------------------------------------------------");
        fftDemonstration();
    }

    private static void dfsDemonstration(){
        List<Complex> input = new ArrayList<>();
        input.add(new Complex(4, 0));
        input.add(new Complex(2, 0));
        input.add(new Complex(1, 0));
        input.add(new Complex(4, 0));
        input.add(new Complex(6, 0));
        input.add(new Complex(3, 0));
        input.add(new Complex(5, 0));
        input.add(new Complex(2, 0));
        List<Complex> output = FourierTransformation.dft(input);

        input.forEach(x -> System.out.println(x + "  "));
        System.out.println();
        output.forEach(x -> System.out.println(x + "  "));
    }

    private static void fftDemonstration(){

        List<Complex> input = new ArrayList<>();
        input.add(new Complex(5, 0));
        input.add(new Complex(7, 0));
        input.add(new Complex(3, 0));
        input.add(new Complex(3, 0));
        input.add(new Complex(0, 0));
        input.add(new Complex(0, 0));
        input.add(new Complex(15, 0));
        input.add(new Complex(32, 0));
        List<Complex> output = FourierTransformation.fft(input);

        input.forEach(x -> System.out.println(x + "  "));
        System.out.println();
        output.forEach(x -> System.out.println(x + "  "));
    }

}
