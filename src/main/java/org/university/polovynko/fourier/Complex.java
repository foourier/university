package org.university.polovynko.fourier;


public class Complex extends Number{
    public double real;
    public double imagine;

    public Complex(double real, double imagine) {
        this.real = real;
        this.imagine = imagine;
    }

    public Complex() {
    }

    @Override
    public String toString() {
        return imagine > 0 ? "" + real + " + " + imagine + "i" : "" + real + " - " + Math.abs(imagine) + "i";
    }

    @Override
    public int intValue() {
        return (int)Math.sqrt(Math.pow(real, 2) + Math.pow(imagine, 2));
    }

    @Override
    public long longValue() {
        return (long)Math.sqrt(Math.pow(real, 2) + Math.pow(imagine, 2));
    }

    @Override
    public float floatValue() {
        return (float) Math.sqrt(Math.pow(real, 2) + Math.pow(imagine, 2));
    }

    @Override
    public double doubleValue() {
        return Math.sqrt(Math.pow(real, 2) + Math.pow(imagine, 2));
    }
}
