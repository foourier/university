package org.university.polovynko.fourier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FourierTransformation {
    public static List<Complex> dft(List<Complex> input){
        List<Complex> output = new ArrayList<>();
        int k, n;
        double WN, wk,c,s;
        WN=2*Math.PI/input.size();
        Complex x;
        for(k=0;k<input.size()-1;k++){
            x = new Complex(0, 0);
            wk=k*WN;
            for(n=0;n<input.size()-1;n++){
                c=Math.cos(n*wk);
                s=Math.sin(n*wk);
                x.real = x.real + input.get(n+1).real * c + input.get(n+1).imagine * s;
                x.imagine = x.imagine - input.get(n+1).real * s + input.get(n+1).imagine * c;
            }
            output.add(x);
        }
        return output;
    }

    public static List<Complex> fft(List<Complex> input){
        List<Complex> output = new ArrayList<>();
        for (Complex x : input)
            output.add(new Complex(x.real, x.imagine));

        int sign;
        int m,irem,l,le,le1,k,ip,i,j;
        double ur,ui,wr,wi,tr,ti,temp;
        int npt = output.size();
        j=1;
        for(i=1;i<npt;++i) {
            if(i<j) {
                tr=output.get(j).real;
                ti=output.get(j).imagine;
                output.get(j).real=output.get(i).real;
                output.get(j).imagine=output.get(i).imagine;
                output.get(i).real=tr;
                output.get(i).imagine=ti;
                k=output.size()/2;
                while(k<j) {
                    j=j-k;
                    k=k/2;
                }
            }
            else{
                k=npt/2;
                while(k<j)
                {
                    j=j-k;
                    k=k/2;
                }
            }
            j=j+k;
        }
        m=0;irem=npt;
        while(irem>1) {
            irem=irem/2;
            m=m+1;
        }

        sign=-1;
        for(l=1;l<=m;++l) {
            le=(int)Math.pow(2,l);
            le1=le/2;
            ur=1.0; ui=0;
            wr=Math.cos(Math.PI/le1);
            wi=sign*Math.sin(Math.PI/le1);
            for(j=1;j<=le1;++j){
                i=j;
                while(i<npt){
                    ip=i+le1;
                    if(ip == npt)
                        break;
                    tr=output.get(ip).real*ur-output.get(ip).imagine*ui;
                    ti=output.get(ip).imagine*ur+output.get(ip).real*ui;
                    output.get(ip).real=output.get(i).real-tr;
                    output.get(ip).imagine=output.get(i).imagine-ti;
                    output.get(i).real=output.get(i).real+tr;
                    output.get(i).imagine=output.get(i).imagine+ti;
                    i=i+le;
                }
                temp=ur*wr-ui*wi;
                ui=ui*wr+ur*wi;
                ur=temp;
            }
        }
        return output;
    }
}
