package org.university.polovynko.signal;

import org.university.polovynko.signal.operation.*;
import org.university.presentation.ShowGistagram;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
//        int[] firstSignal = new int[]{2, 6, 5, 4, 7, 6, 5, 8, 4};
//        int[] secondSignal = new int[]{1, 8, 4, 3, 5, 0, 9};

        int[] firstSignal = new int[]{1, 6, 5, 0, 9, 4, 7 };
        int[] secondSignal = new int[]{8, 7, 3, 2, 4, 5 };

        ShowGistagram.show("First signal",
                Arrays.stream(firstSignal).mapToDouble(x -> x).boxed().collect(Collectors.toList()),
                Stream
                        .iterate(1, a -> a+1)
                        .limit(firstSignal.length)
                        .mapToDouble(x->x)
                        .boxed()
                        .collect(Collectors.toList())
        );

        ShowGistagram.show("Second signal",
                Arrays.stream(secondSignal).mapToDouble(x -> x).boxed().collect(Collectors.toList()),
                Stream
                        .iterate(1, a -> a+1)
                        .limit(secondSignal.length)
                        .mapToDouble(x->x)
                        .boxed()
                        .collect(Collectors.toList())
        );

        Operation scaling = new Scaling(firstSignal, 3);
        Operation timeRevers = new TimeRevers(firstSignal);
        Operation timeOffset = new TimeOffset(firstSignal, 2);
        Operation expantion = new Expansion(firstSignal, 2);
        Operation sum = new Sum(firstSignal, secondSignal);
        Operation mult = new Multiplication(firstSignal, secondSignal);

        List<Double> count = Stream
                .iterate(1, a -> a+1)
                .limit((firstSignal.length > secondSignal.length)?firstSignal.length : secondSignal.length)
                .mapToDouble(x->x)
                .boxed()
                .collect(Collectors.toList());

        ShowGistagram.show(scaling.getClass().getName(),
                scaling.doOperation().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                count);
        ShowGistagram.show(timeOffset.getClass().getName(),
                timeOffset.doOperation().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                count);
        ShowGistagram.show(timeRevers.getClass().getName(),
                timeRevers.doOperation().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                count);
        ShowGistagram.show(sum.getClass().getName(),
                sum.doOperation().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                count);
        ShowGistagram.show(mult.getClass().getName(),
                mult.doOperation().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                count);
        ShowGistagram.show(expantion.getClass().getName(),
                expantion.doOperation().stream().mapToDouble(x->x).boxed().collect(Collectors.toList()),
                count);

    }



}
