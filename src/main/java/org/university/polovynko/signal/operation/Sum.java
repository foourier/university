package org.university.polovynko.signal.operation;

import java.util.ArrayList;
import java.util.List;

public class Sum implements Operation {
    private int[] firstSignal;
    private int[] secondSignal;

    public Sum(int[] firstSignal, int[] secondSignal) {
        this.firstSignal = firstSignal;
        this.secondSignal = secondSignal;
    }

    @Override
    public List<Integer> doOperation() {
        List<Integer> result = new ArrayList<>();
        if (firstSignal.length > secondSignal.length){
            for (int i=0; i<secondSignal.length; i++){
                result.add(firstSignal[i] + secondSignal[i]);
            }
            for(int i=secondSignal.length; i<firstSignal.length; i++){
                result.add(firstSignal[i]);
            }
        }else if(firstSignal.length < secondSignal.length){
            for (int i=0; i<firstSignal.length; i++){
                result.add(firstSignal[i] + secondSignal[i]);
            }
            for(int i=firstSignal.length; i<secondSignal.length; i++){
                result.add(firstSignal[i]);
            }
        }else {
            for (int i=0; i<firstSignal.length; i++){
                result.add(firstSignal[i] + secondSignal[i]);
            }
        }
        return result;
    }
}
