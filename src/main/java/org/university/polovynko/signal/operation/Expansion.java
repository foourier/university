package org.university.polovynko.signal.operation;

import java.util.ArrayList;
import java.util.List;

public class Expansion implements Operation {
    private int[] signal;
    private int alpha;

    public Expansion(int[] signal, int alpha) {
        this.signal = signal;
        this.alpha = alpha;
    }

    @Override
    public List<Integer> doOperation() {
        List<Integer> result = new ArrayList<>();
        for (int i=0; i<signal.length/alpha; i++){
            result.add(signal[i*alpha]);
        }
        return result;
    }
}
