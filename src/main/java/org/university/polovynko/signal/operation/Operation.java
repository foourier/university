package org.university.polovynko.signal.operation;

import java.util.List;

@FunctionalInterface
public interface Operation {
    List<Integer> doOperation();
}
