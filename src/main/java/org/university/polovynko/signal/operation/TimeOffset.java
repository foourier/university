package org.university.polovynko.signal.operation;

import java.util.ArrayList;
import java.util.List;

public class TimeOffset implements Operation {
    private int[] signal;
    private int n;

    public TimeOffset(int[] signal, int n) {
        this.signal = signal;
        this.n = n;
    }

    @Override
    public List<Integer> doOperation() {
        List<Integer> result = new ArrayList<>();
        if (n > 0) {
            for (int i = n; i < signal.length; i++) {
                result.add(signal[i]);
            }
        }
        return result;
    }
}
