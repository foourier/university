package org.university.polovynko.signal.operation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TimeRevers implements Operation{
    private int[] signal;

    public TimeRevers(int[] signal) {
        this.signal = signal;
    }

    @Override
    public List<Integer> doOperation() {
        List<Integer> result = Arrays.stream(signal).boxed().collect(Collectors.toList());
        Collections.reverse(result);
        return result;
    }
}
