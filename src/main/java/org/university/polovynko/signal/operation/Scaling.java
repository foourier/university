package org.university.polovynko.signal.operation;

import java.util.ArrayList;
import java.util.List;

public class Scaling implements Operation {
    private int[] signal;
    private int alpha;

    public  Scaling(int[] signal, int alpha){
        this.signal = signal;
        this.alpha = alpha;
    }

    @Override
    public List<Integer> doOperation() {
        List<Integer> result = new ArrayList<>();
        for (int i: signal)
            result.add(i*alpha);
        return result;
    }
}
